from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import sys
import numpy as np
import tensorflow as tf
import argparse
import os
import re
import scipy
from scipy import ndimage
from PIL import Image
from functools import partial

from eval import compute_map
#import models

tf.logging.set_verbosity(tf.logging.INFO)

CLASS_NAMES = [
    'aeroplane',
    'bicycle',
    'bird',
    'boat',
    'bottle',
    'bus',
    'car',
    'cat',
    'chair',
    'cow',
    'diningtable',
    'dog',
    'horse',
    'motorbike',
    'person',
    'pottedplant',
    'sheep',
    'sofa',
    'train',
    'tvmonitor',
]

def prep_data_augment(image):
    image = tf.random_crop(image, [224, 224, 3])
    image = tf.image.random_flip_left_right(image)
    return image

def cnn_model_fn(features, labels, mode, num_classes=20):
    # Write this function
    """Model function for CNN."""
    # kernel_initializer
    #kernel_initializer=tf.random_normal_initializer(mean=0.0,stddev=0.01)
    # bias_initializer
    bias_initializer=tf.zeros_initializer()
    
    # Input
    input_layer = tf.reshape(features["x"], [-1, 256, 256, 3])
    input_weights = tf.reshape(features["w"], [-1, 20])
     
    # Data Augmentation
    if mode == tf.estimator.ModeKeys.TRAIN:
        input_layer = tf.map_fn(prep_data_augment, input_layer)
    else:
        input_layer = tf.image.resize_image_with_crop_or_pad(input_layer, 224, 224)
    
    tf.summary.image("sample_image", input_layer, max_outputs=3)
    # Block 1
    block1_conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=64,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        bias_initializer=bias_initializer)
    
    block1_conv2 = tf.layers.conv2d(
        inputs=block1_conv1,
        filters=64,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        bias_initializer=bias_initializer)
    
    block1_pool = tf.layers.max_pooling2d(inputs=block1_conv2, pool_size=[2, 2], strides=2)

    # Block 2
    block2_conv1 = tf.layers.conv2d(
        inputs=block1_pool,
        filters=128,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        bias_initializer=bias_initializer)
    
    block2_conv2 = tf.layers.conv2d(
        inputs=block2_conv1,
        filters=128,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        bias_initializer=bias_initializer)
    
    block2_pool = tf.layers.max_pooling2d(inputs=block2_conv2, pool_size=[2, 2], strides=2)
    
    # Block 3
    block3_conv1 = tf.layers.conv2d(
        inputs=block2_pool,
        filters=256,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        bias_initializer=bias_initializer)
    
    block3_conv2 = tf.layers.conv2d(
        inputs=block3_conv1,
        filters=256,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        bias_initializer=bias_initializer)
    
    block3_conv3 = tf.layers.conv2d(
        inputs=block3_conv2,
        filters=256,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        bias_initializer=bias_initializer)
    
    block3_pool = tf.layers.max_pooling2d(inputs=block3_conv3, pool_size=[2, 2], strides=2)
    
    # Block 4
    block4_conv1 = tf.layers.conv2d(
        inputs=block3_pool,
        filters=512,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        bias_initializer=bias_initializer)
    
    block4_conv2 = tf.layers.conv2d(
        inputs=block4_conv1,
        filters=512,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        bias_initializer=bias_initializer)
    
    block4_conv3 = tf.layers.conv2d(
        inputs=block4_conv2,
        filters=512,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        bias_initializer=bias_initializer)
    
    block4_pool = tf.layers.max_pooling2d(inputs=block4_conv3, pool_size=[2, 2], strides=2)
    
    # Block 5
    block5_conv1 = tf.layers.conv2d(
        inputs=block4_pool,
        filters=512,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        bias_initializer=bias_initializer)
    
    block5_conv2 = tf.layers.conv2d(
        inputs=block5_conv1,
        filters=512,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        bias_initializer=bias_initializer)
    
    block5_conv3 = tf.layers.conv2d(
        inputs=block5_conv2,
        filters=512,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        bias_initializer=bias_initializer)
    
    block5_pool = tf.layers.max_pooling2d(inputs=block5_conv3, pool_size=[2, 2], strides=2)

    # Dense Layer
    flatten = tf.reshape(block5_pool, [-1, 7 * 7 * 512])
    dense1 = tf.layers.dense(inputs=flatten, units=4096,
                            activation=tf.nn.relu,bias_initializer=bias_initializer)
    
    dropout1 = tf.layers.dropout(
        inputs=dense1, rate=0.5, training=mode == tf.estimator.ModeKeys.TRAIN)
    
    dense2 = tf.layers.dense(inputs=dropout1, units=4096,
                            activation=tf.nn.relu,bias_initializer=bias_initializer)
    
    dropout2 = tf.layers.dropout(
        inputs=dense2, rate=0.5, training=mode == tf.estimator.ModeKeys.TRAIN)
    
    logits = tf.layers.dense(inputs=dropout2, units=20,bias_initializer=bias_initializer)
    
    predictions = {
        "classes": tf.to_int32(logits>0.5),
        "probabilities": tf.nn.sigmoid(logits, name="sigmoid_tensor")
    }
        
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)
    
    loss = tf.identity(tf.losses.sigmoid_cross_entropy(
        multi_class_labels=labels, logits=logits, weights = input_weights), name='loss')
        
    # Configure the Training Op (for TRAIN mode)
    if mode == tf.estimator.ModeKeys.TRAIN: 
        #leanrning rate
        learning_rate = tf.train.exponential_decay(0.001, tf.train.get_global_step(),10000, 0.5, staircase=True)
        tf.summary.scalar("learning_rate", learning_rate)
        grads = tf.gradients(loss, tf.trainable_variables())
        grads = list(zip(grads, tf.trainable_variables()))
        for grad, var in grads:
            tf.summary.histogram("{}/grad_histogram".format(var.name), grad)

        optimizer = tf.train.MomentumOptimizer(learning_rate=learning_rate,momentum=0.9)
        train_op = optimizer.minimize(loss=loss,global_step=tf.train.get_global_step())        
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op) 
    
    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(
            labels=labels, predictions=predictions["classes"])}
    return tf.estimator.EstimatorSpec(
        mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)   
    
def load_pascal(data_dir, split):
    """
    Function to read images from PASCAL data folder.
    Args:
        data_dir (str): Path to the VOC2007 directory.
        split (str): train/val/trainval split to use.
    Returns:
        images (np.ndarray): Return a np.float32 array of
            shape (N, H, W, 3), where H, W are 224px each,
            and each image is in RGB format.
        labels (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are active in that image.
        weights: (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are confidently labeled and 0s for classes that 
            are ambiguous.
    """
    # Wrote this function
    
    namelist=[]
    im={}
    path=os.path.join(data_dir,'ImageSets/Main')
    for filename in os.listdir(path):
        namelist.append(re.split('_|\.',filename))
    for ele in namelist:
        if len(ele)==3 and ele[1]==split:
            tempfile=open(os.path.join(path,''.join(ele[0]+'_'+ele[1]+'.'+ele[2])))
            for line in tempfile:
                line=line.strip('\n')
                tempi=line.split()
                if tempi[0] in im:
                    if tempi[1]=='1':    
                        im[tempi[0]][0][0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        im[tempi[0]][1][0][CLASS_NAMES.index(ele[0])]=1
                else:
                    a=np.zeros((1,20),dtype=np.int32)
                    b=np.zeros((1,20),dtype=np.int32)
                    if tempi[1]=='1':    
                        a[0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        b[0][CLASS_NAMES.index(ele[0])]=1
                    im[tempi[0]]=[a,b]
            tempfile.close()
    
    image_name=list(im.keys())
    N=len(im)
    H=256
    W=256
    
    image=np.zeros((N,H,W,3),dtype=np.float32)
    labels=np.zeros((N,20),dtype=np.int32)
    weights=np.zeros((N,20),dtype=np.int32)
    for i in range(0,N): 
        temp_image_path=os.path.join(data_dir,'JPEGImages/'+image_name[i]+'.jpg')
        tempimage = np.array(ndimage.imread(temp_image_path, flatten=False)) 
        image[i,:,:,:]=scipy.misc.imresize(tempimage, size=(256,256))
        labels[i,:]=im[image_name[i]][0]
        weights[i,:]=im[image_name[i]][1]
    
    return image,labels,weights

def parse_args():
    parser = argparse.ArgumentParser(
        description='Train a classifier in tensorflow!')
    parser.add_argument(
        'data_dir', type=str, default='data/VOC2007',
        help='Path to PASCAL data storage')
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    return args

def _get_el(arr, i):
    try:
        return arr[i]
    except IndexError:
        return arr

def main():
    NUM_ITERS=400
    BATCH_SIZE=10
    args = parse_args()
    # Load training and eval data
    train_data, train_labels, train_weights = load_pascal(
        args.data_dir, split='trainval')
    eval_data, eval_labels, eval_weights = load_pascal(
        args.data_dir, split='test')

    pascal_classifier = tf.estimator.Estimator(
        model_fn=partial(cnn_model_fn,
                         num_classes=train_labels.shape[1]),
        model_dir="hw1_summary/pascal_model_vgg16",config=tf.estimator.RunConfig(save_checkpoints_steps=NUM_ITERS,save_summary_steps=NUM_ITERS))
    
    tensors_to_log = {"loss": "loss"}
    logging_hook = tf.train.LoggingTensorHook(
        tensors=tensors_to_log, every_n_iter=50)

    # Train the model
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": train_data, "w": train_weights},
        y=train_labels,
        batch_size=BATCH_SIZE,
        num_epochs=None,
        shuffle=True)
    
    sess = tf.Session()
    writer = tf.summary.FileWriter("hw1_summary/mAP_vgg16")
    temp_mAP = tf.Variable(initial_value=0, dtype=tf.float32)
    mAP = tf.summary.scalar("mAP", temp_mAP)
    init = tf.initialize_all_variables()
    sess.run(init)

    # Evaluate the model and print results
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": eval_data, "w": eval_weights},
        y=eval_labels,
        num_epochs=1,
        shuffle=False)
    for i in range(0,100):
        pascal_classifier.train(
            input_fn=train_input_fn,
            steps=NUM_ITERS,
            hooks=[logging_hook])
        pred = list(pascal_classifier.predict(input_fn=eval_input_fn))
        pred = np.stack([p['probabilities'] for p in pred])
        AP = compute_map(eval_labels, pred, eval_weights, average=None)
        print('Obtained {} mAP'.format(np.mean(AP)))
        update = tf.assign(temp_mAP, np.mean(AP))
        sess.run(update)
        writer.add_summary(sess.run(mAP), i*NUM_ITERS)
        
if __name__ == "__main__":
    main()
