from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import sys
import numpy as np
import tensorflow as tf
import argparse
import os
import re
import scipy
from scipy import ndimage
from PIL import Image
from functools import partial

from eval import compute_map
#import models
import csv

tf.logging.set_verbosity(tf.logging.INFO)

CLASS_NAMES = [
    'aeroplane',
    'bicycle',
    'bird',
    'boat',
    'bottle',
    'bus',
    'car',
    'cat',
    'chair',
    'cow',
    'diningtable',
    'dog',
    'horse',
    'motorbike',
    'person',
    'pottedplant',
    'sheep',
    'sofa',
    'train',
    'tvmonitor',
]


def cnn_model_fn(features, labels, mode, num_classes=20):
    # Write this function
    """Model function for CNN."""
    # Input Layer
    input_layer = tf.reshape(features["x"], [-1, 256, 256, 3])

    # Convolutional Layer #1
    conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=32,
        kernel_size=[5, 5],
        padding="same",
        activation=tf.nn.relu)

    # Pooling Layer #1
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

    # Convolutional Layer #2 and Pooling Layer #2
    conv2 = tf.layers.conv2d(
        inputs=pool1,
        filters=64,
        kernel_size=[5, 5],
        padding="same",
        activation=tf.nn.relu)
    pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)

    # Dense Layer
    pool2_flat = tf.reshape(pool2, [-1, 64 * 64 * 64])
    dense = tf.layers.dense(inputs=pool2_flat, units=1024,
                            activation=tf.nn.relu)
    dropout = tf.layers.dropout(
        inputs=dense, rate=0.4, training=mode == tf.estimator.ModeKeys.TRAIN)

    # Logits Layer
    logits = tf.layers.dense(inputs=dropout, units=20)

    predictions = {
        "probabilities": tf.nn.sigmoid(logits, name="sigmoid_tensor")
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Calculate Loss (for TRAIN mode)
    loss = tf.identity(tf.losses.sigmoid_cross_entropy(
        multi_class_labels=labels, logits=logits), name='loss')

    # Configure the Training Op (for TRAIN mode)
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
        train_op = optimizer.minimize(
            loss=loss,
            global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(
            mode=mode, loss=loss, train_op=train_op)

def load_pascal(data_dir, split):
    """
    Function to read images from PASCAL data folder.
    Args:
        data_dir (str): Path to the VOC2007 directory.
        split (str): train/val/trainval split to use.
    Returns:
        images (np.ndarray): Return a np.float32 array of
            shape (N, H, W, 3), where H, W are 224px each,
            and each image is in RGB format.
        labels (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are active in that image.
        weights: (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are confidently labeled and 0s for classes that 
            are ambiguous.
    """
    # Wrote this function
    
    namelist=[]
    im={}
    path=os.path.join(data_dir,'ImageSets/Main')
    for filename in os.listdir(path):
        namelist.append(re.split('_|\.',filename))
    for ele in namelist:
        if len(ele)==3 and ele[1]==split:
            tempfile=open(os.path.join(path,''.join(ele[0]+'_'+ele[1]+'.'+ele[2])))
            for line in tempfile:
                line=line.strip('\n')
                tempi=line.split()
                if tempi[0] in im:
                    if tempi[1]=='1':    
                        im[tempi[0]][0][0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        im[tempi[0]][1][0][CLASS_NAMES.index(ele[0])]=1
                else:
                    a=np.zeros((1,20),dtype=np.int32)
                    b=np.zeros((1,20),dtype=np.int32)
                    if tempi[1]=='1':    
                        a[0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        b[0][CLASS_NAMES.index(ele[0])]=1
                    im[tempi[0]]=[a,b]
            tempfile.close()
    
    image_name=list(im.keys())
    N=len(im)
    H=256
    W=256
    
    image=np.zeros((N,H,W,3),dtype=np.float32)
    labels=np.zeros((N,20),dtype=np.int32)
    weights=np.zeros((N,20),dtype=np.int32)
    for i in range(0,N): 
        temp_image_path=os.path.join(data_dir,'JPEGImages/'+image_name[i]+'.jpg')
        tempimage = np.array(ndimage.imread(temp_image_path, flatten=False)) 
        image[i,:,:,:]=scipy.misc.imresize(tempimage, size=(256,256))
        labels[i,:]=im[image_name[i]][0]
        weights[i,:]=im[image_name[i]][1]
    
    return image,labels,weights

def parse_args():
    parser = argparse.ArgumentParser(
        description='Train a classifier in tensorflow!')
    parser.add_argument(
        'data_dir', type=str, default='data/VOC2007',
        help='Path to PASCAL data storage')
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    return args

def _get_el(arr, i):
    try:
        return arr[i]
    except IndexError:
        return arr

def main():
    mAP=[]
    gstep=[]
    NUM_ITERS=10
    BATCH_SIZE=50
    args = parse_args()
    #data_dir='/Volumes/Users/jianrenw/Documents/VOCdevkit/VOC2007'
    # Load training and eval data
    train_data, train_labels, train_weights = load_pascal(
        args.data_dir, split='trainval')
    eval_data, eval_labels, eval_weights = load_pascal(
        args.data_dir, split='test')

    pascal_classifier = tf.estimator.Estimator(
        model_fn=partial(cnn_model_fn,
                         num_classes=train_labels.shape[1]),
        model_dir="./pascal_model_scratch")
    tensors_to_log = {"loss": "loss"}
    logging_hook = tf.train.LoggingTensorHook(
        tensors=tensors_to_log, every_n_iter=10)
    # Train the model
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": train_data, "w": train_weights},
        y=train_labels,
        batch_size=BATCH_SIZE,
        num_epochs=None,
        shuffle=True)
    # Evaluate the model and print results
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": eval_data, "w": eval_weights},
        y=eval_labels,
        num_epochs=1,
        shuffle=False)
    for i in range(0,100):
        pascal_classifier.train(
            input_fn=train_input_fn,
            steps=NUM_ITERS,
            hooks=[logging_hook])
        pred = list(pascal_classifier.predict(input_fn=eval_input_fn))
        pred = np.stack([p['probabilities'] for p in pred])
# =============================================================================
#         rand_AP = compute_map(
#             eval_labels, np.random.random(eval_labels.shape),
#             eval_weights, average=None)
#         print('Random AP: {} mAP'.format(np.mean(rand_AP)))
#         gt_AP = compute_map(
#             eval_labels, eval_labels, eval_weights, average=None)
#         print('GT AP: {} mAP'.format(np.mean(gt_AP)))
# =============================================================================
        AP = compute_map(eval_labels, pred, eval_weights, average=None)
        gstep.append(i*10)
        mAP.append(np.mean(AP))
        print('Obtained {} mAP'.format(mAP[i]))
# =============================================================================
#         print('per class:')
#         for cid, cname in enumerate(CLASS_NAMES):
#             print('{}: {}'.format(cname, _get_el(AP, cid)))
# =============================================================================
            
    #output data
    output_dir = "./hw1_1.csv" 
    csv = open(output_dir, "w") 
    columnTitleRow = "global_step, mAP\n"
    csv.write(columnTitleRow)
    for i in range(0,len(gstep)):
        global_step = gstep[i]
        temp_mAP = mAP[i]
        row = str(global_step) + "," + str(temp_mAP) + "\n"
        csv.write(row)
    csv.close()

if __name__ == "__main__":
    main()