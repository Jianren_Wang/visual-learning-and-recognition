from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import sys
import numpy as np
import tensorflow as tf
import argparse
import os
import re
import scipy
from scipy import ndimage
import scipy.misc
from PIL import Image
from functools import partial
from tensorflow.python.framework import ops
from sklearn.neighbors import NearestNeighbors
from eval import compute_map
import random

tf.logging.set_verbosity(tf.logging.INFO)

CLASS_NAMES = [
    'aeroplane',
    'bicycle',
    'bird',
    'boat',
    'bottle',
    'bus',
    'car',
    'cat',
    'chair',
    'cow',
    'diningtable',
    'dog',
    'horse',
    'motorbike',
    'person',
    'pottedplant',
    'sheep',
    'sofa',
    'train',
    'tvmonitor',
]

def create_placeholders(n_H0, n_W0, n_C0, n_y):
    """
    Creates the placeholders for the tensorflow session.
    
    Arguments:
    n_H0 -- scalar, height of an input image
    n_W0 -- scalar, width of an input image
    n_C0 -- scalar, number of channels of the input
    n_y -- scalar, number of classes
        
    Returns:
    X -- placeholder for the data input, of shape [None, n_H0, n_W0, n_C0] and dtype "float"
    Y -- placeholder for the input labels, of shape [None, n_y] and dtype "float"
    """   
    X = tf.placeholder(tf.float32, [None, n_H0, n_W0, n_C0])
    Y = tf.placeholder(tf.float32, [None, n_y]) 
    return X, Y

def model(X_test, Y_test):
    
    ops.reset_default_graph()# to be able to return the model without overwriting tf variables
    (m, n_H0, n_W0, n_C0) = X_test.shape
    n_y = Y_test.shape[1]
    input_layer = tf.image.resize_image_with_crop_or_pad(X_test, 224, 224)
    
    with tf.variable_scope("vgg_16") as scope:
        with tf.variable_scope("conv1") as scope:
            # Convolutional Layer #1_1
            conv1_1 = tf.layers.conv2d(
                inputs=input_layer,
                filters=64,
                kernel_size=[3, 3],
                padding="same",
                activation=tf.nn.relu,
                name="conv1_1")

            # Convolutional Layer #1_2
            conv1_2 = tf.layers.conv2d(
                inputs=conv1_1,
                filters=64,
                kernel_size=[3, 3],
                padding="same",
                activation=tf.nn.relu,
                name="conv1_2")

        with tf.variable_scope("pool1") as scope:
            # Pooling Layer #1
            pool1 = tf.layers.max_pooling2d(inputs=conv1_2, pool_size=[2, 2], strides=2, name="pool1")

        with tf.variable_scope("conv2") as scope:
            # Convolutional Layer #2_1
            conv2_1 = tf.layers.conv2d(
                inputs=pool1,
                filters=128,
                kernel_size=[3, 3],
                padding="same",
                activation=tf.nn.relu,
                name="conv2_1")

            # Convolutional Layer #2_2
            conv2_2 = tf.layers.conv2d(
                inputs=conv2_1,
                filters=128,
                kernel_size=[3, 3],
                padding="same",
                activation=tf.nn.relu,
                name="conv2_2")

        with tf.variable_scope("pool2") as scope:
            # Pooling Layer #2
            pool2 = tf.layers.max_pooling2d(inputs=conv2_2, pool_size=[2, 2], strides=2)

        with tf.variable_scope("conv3") as scope:
            # Convolutional Layer #3_1
            conv3_1 = tf.layers.conv2d(
                inputs=pool2,
                filters=256,
                kernel_size=[3, 3],
                padding="same",
                activation=tf.nn.relu,
                name="conv3_1")

            # Convolutional Layer #3_2
            conv3_2 = tf.layers.conv2d(
                inputs=conv3_1,
                filters=256,
                kernel_size=[3, 3],
                padding="same",
                activation=tf.nn.relu,
                name="conv3_2")

            # Convolutional Layer #3_3
            conv3_3 = tf.layers.conv2d(
                inputs=conv3_2,
                filters=256,
                kernel_size=[3, 3],
                padding="same",
                activation=tf.nn.relu,
                name="conv3_3")

        with tf.variable_scope("pool3") as scope:
            # Pooling Layer #3
            pool3 = tf.layers.max_pooling2d(inputs=conv3_3, pool_size=[2, 2], strides=2, name="pool3")

        with tf.variable_scope("conv4") as scope:
            # Convolutional Layer #4_1
            conv4_1 = tf.layers.conv2d(
                inputs=pool3,
                filters=512,
                kernel_size=[3, 3],
                padding="same",
                activation=tf.nn.relu,
                name="conv4_1")

            # Convolutional Layer #4_2
            conv4_2 = tf.layers.conv2d(
                inputs=conv4_1,
                filters=512,
                kernel_size=[3, 3],
                padding="same",
                activation=tf.nn.relu,
                name="conv4_2")

            # Convolutional Layer #4_3
            conv4_3 = tf.layers.conv2d(
                inputs=conv4_2,
                filters=512,
                kernel_size=[3, 3],
                padding="same",
                activation=tf.nn.relu,
                name="conv4_3")

        with tf.variable_scope("pool4") as scope:
            # Pooling Layer #4
            pool4 = tf.layers.max_pooling2d(inputs=conv4_3, pool_size=[2, 2], strides=2, name="pool4")

        with tf.variable_scope("conv5") as scope:
            # Convolutional Layer #5_1
            conv5_1 = tf.layers.conv2d(
                inputs=pool4,
                filters=512,
                kernel_size=[3, 3],
                padding="same",
                activation=tf.nn.relu,
                name="conv5_1")

            # Convolutional Layer #5_2
            conv5_2 = tf.layers.conv2d(
                inputs=conv5_1,
                filters=512,
                kernel_size=[3, 3],
                padding="same",
                activation=tf.nn.relu,
                name="conv5_2")

            # Convolutional Layer #5_3
            conv5_3 = tf.layers.conv2d(
                inputs=conv5_2,
                filters=512,
                kernel_size=[3, 3],
                padding="same",
                activation=tf.nn.relu,
                name="conv5_3")

        with tf.variable_scope("pool5") as scope:
            # Pooling Layer #5
            pool5 = tf.layers.max_pooling2d(inputs=conv5_3, pool_size=[2, 2], strides=2, name="pool5")

        # Dense Layer
        pool5_flat = tf.reshape(pool5, [-1, 7 * 7 * 512])
        dense1 = tf.layers.dense(
            inputs=pool5_flat,
            units=4096,
            kernel_initializer=tf.random_normal_initializer(0, 0.01),
            bias_initializer=tf.zeros_initializer(),
            activation=tf.nn.relu,
            name="fc6")

        dropout1 = tf.layers.dropout(
            inputs=dense1, rate=0.5, training=False)

        dense2 = tf.layers.dense(
            inputs=dropout1,
            units=4096,
            kernel_initializer=tf.random_normal_initializer(0, 0.01),
            bias_initializer=tf.zeros_initializer(),
            activation=tf.nn.relu,
            name="fc7")
     
    saver = tf.train.Saver()
    
    # Create Placeholders of the correct shape
    X, Y = create_placeholders(n_H0, n_W0, n_C0, n_y)
    sess=tf.Session()
    # Restore variables from disk.
    saver.restore(sess, "hw1_summary/pascal_model_vgg_pretrained/model.ckpt-1080")
    
    # Forward propagation: Build the forward propagation in the tensorflow graph
    
    p5=sess.run([dense2], feed_dict={X:X_test, Y:Y_test})
    
    return p5

def load_pascal(data_dir, split='train'):
    """
    Function to read images from PASCAL data folder.
    Args:
        data_dir (str): Path to the VOC2007 directory.
        split (str): train/val/trainval split to use.
    Returns:
        images (np.ndarray): Return a np.float32 array of
            shape (N, H, W, 3), where H, W are 224px each,
            and each image is in RGB format.
        labels (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are active in that image.
        weights: (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are confidently labeled and 0s for classes that 
            are ambiguous.
    """
    # Wrote this function
    
    namelist=[]
    im={}
    path=os.path.join(data_dir,'ImageSets/Main')
    for filename in os.listdir(path):
        namelist.append(re.split('_|\.',filename))
    for ele in namelist:
        if len(ele)==3 and ele[1]==split:
            tempfile=open(os.path.join(path,''.join(ele[0]+'_'+ele[1]+'.'+ele[2])))
            for line in tempfile:
                line=line.strip('\n')
                tempi=line.split()
                if tempi[0] in im:
                    if tempi[1]=='1':    
                        im[tempi[0]][0][0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        im[tempi[0]][1][0][CLASS_NAMES.index(ele[0])]=1
                else:
                    a=np.zeros((1,20),dtype=np.int32)
                    b=np.zeros((1,20),dtype=np.int32)
                    if tempi[1]=='1':    
                        a[0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        b[0][CLASS_NAMES.index(ele[0])]=1
                    im[tempi[0]]=[a,b]
            tempfile.close()
    
    image_name=list(im.keys())
    N=len(im)
    H=256
    W=256
    
    image=np.zeros((N,H,W,3),dtype=np.float32)
    labels=np.zeros((N,20),dtype=np.int32)
    weights=np.zeros((N,20),dtype=np.int32)
    for i in range(0,N): 
        temp_image_path=os.path.join(data_dir,'JPEGImages/'+image_name[i]+'.jpg')
        tempimage = np.array(ndimage.imread(temp_image_path, flatten=False)) 
        image[i,:,:,:]=scipy.misc.imresize(tempimage, size=(256,256))
        labels[i,:]=im[image_name[i]][0]
        weights[i,:]=im[image_name[i]][1]
    
    return image,labels,weights

def parse_args():
    parser = argparse.ArgumentParser(
        description='Train a classifier in tensorflow!')
    parser.add_argument(
        'data_dir', type=str, default='data/VOC2007',
        help='Path to PASCAL data storage')
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    return args

def _get_el(arr, i):
    try:
        return arr[i]
    except IndexError:
        return arr

data_dir='VOCdevkit/VOC2007/'
origin=[]
near=[]
t=[]
eval_data, eval_labels, eval_weights = load_pascal(
    data_dir, split='test')

size=eval_data.shape[0]
layer=np.zeros((size,4096))
k=int(size/100)
m=size%100
for i in range(0,k):
    d2 = model(X_test=eval_data[i*100:i*100+100,:,:,:], Y_test=eval_labels[i*100:i*100+100,:])
    d2 = d2[0]
    layer[i*100:i*100+100,:] = d2

d2 = model(X_test=eval_data[size-m:size,:,:,:], Y_test=eval_labels[size-m:size,:])
d2 = d2[0]
layer[size-m:size,:] = d2

for i in range(0,20):
    for j in range(0,size):
        if eval_labels[j,:][i]==1:
            if j not in t: 
                t.append(j)
                break
            
for i in t:
    test=layer[i, :]
    samples=np.delete(layer,i,axis=0)
    neigh = NearestNeighbors(n_neighbors=1)
    neigh.fit(samples) 
    nearest=neigh.kneighbors(test.reshape(1,-1))[1][0][0]
    if nearest>=i:
        nearest=nearest+1
    near.append(nearest)

print(t)
print(near)

for i in t:
    scipy.misc.imsave(str(i)+'origin.jpg', eval_data[i,:,:,:])
for i in near:
    scipy.misc.imsave(str(i)+'near.jpg', eval_data[i,:,:,:])


    
