from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import sys
import numpy as np
import tensorflow as tf
import argparse
import os
import re
import scipy
from scipy import ndimage
from PIL import Image
from functools import partial

from eval import compute_map
#import models

tf.logging.set_verbosity(tf.logging.INFO)

CLASS_NAMES = [
    'aeroplane',
    'bicycle',
    'bird',
    'boat',
    'bottle',
    'bus',
    'car',
    'cat',
    'chair',
    'cow',
    'diningtable',
    'dog',
    'horse',
    'motorbike',
    'person',
    'pottedplant',
    'sheep',
    'sofa',
    'train',
    'tvmonitor',
]

def prep_data_augment(image):
    image = tf.random_crop(image, [224, 224, 3])
    image = tf.image.random_flip_left_right(image)
    return image

def cnn_model_fn(features, labels, mode, num_classes=20):
    # Input
    input_layer = tf.reshape(features["x"], [-1, 256, 256, 3])
    input_weights = tf.reshape(features["w"], [-1, 20])
     
    # Data Augmentation
    if mode == tf.estimator.ModeKeys.TRAIN:
        input_layer = tf.map_fn(prep_data_augment, input_layer)
    else:
        input_layer = tf.image.resize_image_with_crop_or_pad(input_layer, 224, 224)
        
    with tf.variable_scope("vgg_16") as scope:
        with tf.variable_scope("conv1") as scope:
            # Convolutional Layer #1_1
            conv1_1 = tf.layers.conv2d(
                inputs=input_layer,
                filters=64,
                kernel_size=[3, 3],
                padding="same",
                kernel_initializer=tf.random_normal_initializer(0, 0.01),
                bias_initializer=tf.zeros_initializer(),
                activation=tf.nn.relu,
                name="conv1_1")

            # Convolutional Layer #1_2
            conv1_2 = tf.layers.conv2d(
                inputs=conv1_1,
                filters=64,
                kernel_size=[3, 3],
                padding="same",
                kernel_initializer=tf.random_normal_initializer(0, 0.01),
                bias_initializer=tf.zeros_initializer(),
                activation=tf.nn.relu,
                name="conv1_2")

        with tf.variable_scope("pool1") as scope:
            # Pooling Layer #1
            pool1 = tf.layers.max_pooling2d(inputs=conv1_2, pool_size=[2, 2], strides=2, name="pool1")

        with tf.variable_scope("conv2") as scope:
            # Convolutional Layer #2_1
            conv2_1 = tf.layers.conv2d(
                inputs=pool1,
                filters=128,
                kernel_size=[3, 3],
                padding="same",
                kernel_initializer=tf.random_normal_initializer(0, 0.01),
                bias_initializer=tf.zeros_initializer(),
                activation=tf.nn.relu,
                name="conv2_1")

            # Convolutional Layer #2_2
            conv2_2 = tf.layers.conv2d(
                inputs=conv2_1,
                filters=128,
                kernel_size=[3, 3],
                padding="same",
                kernel_initializer=tf.random_normal_initializer(0, 0.01),
                bias_initializer=tf.zeros_initializer(),
                activation=tf.nn.relu,
                name="conv2_2")

        with tf.variable_scope("pool2") as scope:
            # Pooling Layer #2
            pool2 = tf.layers.max_pooling2d(inputs=conv2_2, pool_size=[2, 2], strides=2)

        with tf.variable_scope("conv3") as scope:
            # Convolutional Layer #3_1
            conv3_1 = tf.layers.conv2d(
                inputs=pool2,
                filters=256,
                kernel_size=[3, 3],
                padding="same",
                kernel_initializer=tf.random_normal_initializer(0, 0.01),
                bias_initializer=tf.zeros_initializer(),
                activation=tf.nn.relu,
                name="conv3_1")

            # Convolutional Layer #3_2
            conv3_2 = tf.layers.conv2d(
                inputs=conv3_1,
                filters=256,
                kernel_size=[3, 3],
                padding="same",
                kernel_initializer=tf.random_normal_initializer(0, 0.01),
                bias_initializer=tf.zeros_initializer(),
                activation=tf.nn.relu,
                name="conv3_2")

            # Convolutional Layer #3_3
            conv3_3 = tf.layers.conv2d(
                inputs=conv3_2,
                filters=256,
                kernel_size=[3, 3],
                padding="same",
                kernel_initializer=tf.random_normal_initializer(0, 0.01),
                bias_initializer=tf.zeros_initializer(),
                activation=tf.nn.relu,
                name="conv3_3")

        with tf.variable_scope("pool3") as scope:
            # Pooling Layer #3
            pool3 = tf.layers.max_pooling2d(inputs=conv3_3, pool_size=[2, 2], strides=2, name="pool3")

        with tf.variable_scope("conv4") as scope:
            # Convolutional Layer #4_1
            conv4_1 = tf.layers.conv2d(
                inputs=pool3,
                filters=512,
                kernel_size=[3, 3],
                padding="same",
                kernel_initializer=tf.random_normal_initializer(0, 0.01),
                bias_initializer=tf.zeros_initializer(),
                activation=tf.nn.relu,
                name="conv4_1")

            # Convolutional Layer #4_2
            conv4_2 = tf.layers.conv2d(
                inputs=conv4_1,
                filters=512,
                kernel_size=[3, 3],
                padding="same",
                kernel_initializer=tf.random_normal_initializer(0, 0.01),
                bias_initializer=tf.zeros_initializer(),
                activation=tf.nn.relu,
                name="conv4_2")

            # Convolutional Layer #4_3
            conv4_3 = tf.layers.conv2d(
                inputs=conv4_2,
                filters=512,
                kernel_size=[3, 3],
                padding="same",
                kernel_initializer=tf.random_normal_initializer(0, 0.01),
                bias_initializer=tf.zeros_initializer(),
                activation=tf.nn.relu,
                name="conv4_3")

        with tf.variable_scope("pool4") as scope:
            # Pooling Layer #4
            pool4 = tf.layers.max_pooling2d(inputs=conv4_3, pool_size=[2, 2], strides=2, name="pool4")

        with tf.variable_scope("conv5") as scope:
            # Convolutional Layer #5_1
            conv5_1 = tf.layers.conv2d(
                inputs=pool4,
                filters=512,
                kernel_size=[3, 3],
                padding="same",
                kernel_initializer=tf.random_normal_initializer(0, 0.01),
                bias_initializer=tf.zeros_initializer(),
                activation=tf.nn.relu,
                name="conv5_1")

            # Convolutional Layer #5_2
            conv5_2 = tf.layers.conv2d(
                inputs=conv5_1,
                filters=512,
                kernel_size=[3, 3],
                padding="same",
                kernel_initializer=tf.random_normal_initializer(0, 0.01),
                bias_initializer=tf.zeros_initializer(),
                activation=tf.nn.relu,
                name="conv5_2")

            # Convolutional Layer #5_3
            conv5_3 = tf.layers.conv2d(
                inputs=conv5_2,
                filters=512,
                kernel_size=[3, 3],
                padding="same",
                kernel_initializer=tf.random_normal_initializer(0, 0.01),
                bias_initializer=tf.zeros_initializer(),
                activation=tf.nn.relu,
                name="conv5_3")

        with tf.variable_scope("pool5") as scope:
            # Pooling Layer #5
            pool5 = tf.layers.max_pooling2d(inputs=conv5_3, pool_size=[2, 2], strides=2, name="pool5")

        # Dense Layer
        pool5_flat = tf.reshape(pool5, [-1, 7 * 7 * 512])
        dense1 = tf.layers.dense(
            inputs=pool5_flat,
            units=4096,
            kernel_initializer=tf.random_normal_initializer(0, 0.01),
            bias_initializer=tf.zeros_initializer(),
            activation=tf.nn.relu,
            name="fc6")

        dropout1 = tf.layers.dropout(
            inputs=dense1, rate=0.5, training=mode == tf.estimator.ModeKeys.TRAIN)

        dense2 = tf.layers.dense(
            inputs=dropout1,
            units=4096,
            kernel_initializer=tf.random_normal_initializer(0, 0.01),
            bias_initializer=tf.zeros_initializer(),
            activation=tf.nn.relu,
            name="fc7")

        dropout2 = tf.layers.dropout(
            inputs=dense2, rate=0.5, training=mode == tf.estimator.ModeKeys.TRAIN)

        #embed()
        # Logits Layer
        logits = tf.layers.dense(
            inputs=dropout2,
            kernel_initializer=tf.random_normal_initializer(0, 0.01),
            bias_initializer=tf.zeros_initializer(),
            units=num_classes,
            name="fc8")

        predictions = {
            "classes": tf.to_int32(logits>0.5),
            "probabilities": tf.nn.sigmoid(logits, name="sigmoid_tensor")
        }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Calculate Loss (for both TRAIN and EVAL modes)
    loss = tf.identity(tf.losses.sigmoid_cross_entropy(
        multi_class_labels=labels, logits=logits, weights = input_weights), name='loss')
    # Add evaluation metrics
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(labels=labels, predictions=predictions["classes"])}
    variables_to_restore = tf.contrib.framework.get_trainable_variables()
    variables_to_restore = variables_to_restore[:-2]
    scopes = {os.path.dirname(v.name) for v in variables_to_restore }
    tf.train.init_from_checkpoint("new_model.ckpt", {s+'/':s+'/' for s in scopes})

    # Configure the Training Op (for TRAIN mode)
    if mode == tf.estimator.ModeKeys.TRAIN:
        learning_rate = tf.train.exponential_decay(
            learning_rate=0.0001,
            global_step=tf.train.get_global_step(),
            decay_steps=1000,
            decay_rate=0.5,
            staircase=True)
        optimizer = tf.train.MomentumOptimizer(learning_rate=learning_rate, momentum=0.9)
        train_op = optimizer.minimize(
            loss=loss,
            global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(
            mode=mode, loss=loss, train_op=train_op)

    return tf.estimator.EstimatorSpec(
        mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)   
         
def load_pascal(data_dir, split):
    """
    Function to read images from PASCAL data folder.
    Args:
        data_dir (str): Path to the VOC2007 directory.
        split (str): train/val/trainval split to use.
    Returns:
        images (np.ndarray): Return a np.float32 array of
            shape (N, H, W, 3), where H, W are 224px each,
            and each image is in RGB format.
        labels (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are active in that image.
        weights: (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are confidently labeled and 0s for classes that 
            are ambiguous.
    """
    # Wrote this function
    
    namelist=[]
    im={}
    path=os.path.join(data_dir,'ImageSets/Main')
    for filename in os.listdir(path):
        namelist.append(re.split('_|\.',filename))
    for ele in namelist:
        if len(ele)==3 and ele[1]==split:
            tempfile=open(os.path.join(path,''.join(ele[0]+'_'+ele[1]+'.'+ele[2])))
            for line in tempfile:
                line=line.strip('\n')
                tempi=line.split()
                if tempi[0] in im:
                    if tempi[1]=='1':    
                        im[tempi[0]][0][0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        im[tempi[0]][1][0][CLASS_NAMES.index(ele[0])]=1
                else:
                    a=np.zeros((1,20),dtype=np.int32)
                    b=np.zeros((1,20),dtype=np.int32)
                    if tempi[1]=='1':    
                        a[0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        b[0][CLASS_NAMES.index(ele[0])]=1
                    im[tempi[0]]=[a,b]
            tempfile.close()
    
    image_name=list(im.keys())
    N=len(im)
    H=256
    W=256
    
    images=np.zeros((N,H,W,3),dtype=np.float32)
    labels=np.zeros((N,20),dtype=np.int32)
    weights=np.zeros((N,20),dtype=np.int32)
    for i in range(0,N): 
        temp_image_path=os.path.join(data_dir,'JPEGImages/'+image_name[i]+'.jpg')
        tempimage = np.array(ndimage.imread(temp_image_path, flatten=False)) 
        images[i,:,:,:]=scipy.misc.imresize(tempimage, size=(256,256))
        labels[i,:]=im[image_name[i]][0]
        weights[i,:]=im[image_name[i]][1]
        
    mean_rgb = tf.contrib.framework.load_variable('new_model.ckpt', 'vgg_16/mean_rgb')
    images[:,:,:,0] -= mean_rgb[0]
    images[:,:,:,1] -= mean_rgb[1]
    images[:,:,:,2] -= mean_rgb[2]
    
    return images,labels,weights


def parse_args():
    parser = argparse.ArgumentParser(
        description='Train a classifier in tensorflow!')
    parser.add_argument(
        'data_dir', type=str, default='data/VOC2007',
        help='Path to PASCAL data storage')
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    return args


def _get_el(arr, i):
    try:
        return arr[i]
    except IndexError:
        return arr


def main():
    args = parse_args()
    BATCH_SIZE = 10
    NUM_ITERS = 40
    # Load training and eval data
    train_data, train_labels, train_weights = load_pascal(
        args.data_dir, split='train')
    val_data, val_labels, val_weights = load_pascal(
        args.data_dir, split='val')
    eval_data, eval_labels, eval_weights = load_pascal(
        args.data_dir, split='test')

    pascal_classifier = tf.estimator.Estimator(
        model_fn=partial(cnn_model_fn,
                         num_classes=train_labels.shape[1]),
        model_dir="hw1_summary/pascal_model_vgg_pretrained",config=tf.estimator.RunConfig(save_checkpoints_steps=NUM_ITERS,save_summary_steps=NUM_ITERS))
    tensors_to_log = {"loss": "loss"}
    logging_hook = tf.train.LoggingTensorHook(
        tensors=tensors_to_log, every_n_iter=10)
    # Train the model
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": train_data, "w": train_weights},
        y=train_labels,
        batch_size=BATCH_SIZE,
        num_epochs=None,
        shuffle=True)
    
    sess = tf.Session()
    writer = tf.summary.FileWriter("hw1_summary/mAP_vgg_pretrained")
    temp_mAP = tf.Variable(initial_value=0, dtype=tf.float32)
    mAP = tf.summary.scalar("mAP", temp_mAP)
    init = tf.initialize_all_variables()
    sess.run(init)
    
    # Evaluate the model and print results
    val_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": val_data, "w": val_weights},
        y=val_labels,
        num_epochs=1,
        shuffle=False)
    
    for i in range(0, 100):
        #print(i)
        pascal_classifier.train(
            input_fn=train_input_fn,
            steps=NUM_ITERS,
            hooks=[logging_hook])
        pascal_classifier.evaluate(input_fn=val_input_fn)
        pred = list(pascal_classifier.predict(input_fn=val_input_fn))
        pred = np.stack([p['probabilities'] for p in pred])
        AP = compute_map(val_labels, pred, val_weights, average=None)
        print('Obtained {} mAP'.format(np.mean(AP)))
        print('per class:')
        for cid, cname in enumerate(CLASS_NAMES):
            print('{}: {}'.format(cname, _get_el(AP, cid)))
        
        update = tf.assign(temp_mAP, np.mean(AP))
        sess.run(update)
        writer.add_summary(sess.run(mAP), i*NUM_ITERS)
        
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": eval_data, "w": eval_weights},
        y=eval_labels,
        num_epochs=1,
        shuffle=False)
    pascal_classifier.evaluate(input_fn=eval_input_fn)
    pred = list(pascal_classifier.predict(input_fn=eval_input_fn))
    pred = np.stack([p['probabilities'] for p in pred])
    AP = compute_map(eval_labels, pred, eval_weights, average=None)
    print('Obtained {} mAP'.format(np.mean(AP)))
    print('per class:')
    for cid, cname in enumerate(CLASS_NAMES):
        print('{}: {}'.format(cname, _get_el(AP, cid)))

if __name__ == "__main__":
    main()
