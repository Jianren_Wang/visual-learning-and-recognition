import tensorflow as tf
import numpy as np

def rename(checkpoint_dir,dry_run=True):
    checkpoint = tf.train.get_checkpoint_state(checkpoint_dir)
    with tf.Session() as sess:
        for var_name, _ in tf.contrib.framework.list_variables(checkpoint_dir):
            # Load the variable
            var = tf.contrib.framework.load_variable(checkpoint_dir, var_name)
            print(var_name)
# Usage
model_dir = "/Volumes/Users/jianrenw/Documents/16824/hw1_summary/pascal_model_alexnet/model.ckpt-39200"
rename(model_dir,dry_run=False)
