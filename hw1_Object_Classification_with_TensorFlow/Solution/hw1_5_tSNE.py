from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import sys
import numpy as np
import tensorflow as tf
import argparse
import os
import re
import scipy
from scipy import ndimage
import scipy.misc
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from collections import OrderedDict
from tensorflow.python.framework import ops
from operator import itemgetter


tf.logging.set_verbosity(tf.logging.INFO)

CLASS_NAMES = [
    'aeroplane',
    'bicycle',
    'bird',
    'boat',
    'bottle',
    'bus',
    'car',
    'cat',
    'chair',
    'cow',
    'diningtable',
    'dog',
    'horse',
    'motorbike',
    'person',
    'pottedplant',
    'sheep',
    'sofa',
    'train',
    'tvmonitor',
]

def create_placeholders(n_H0, n_W0, n_C0, n_y):
    """
    Creates the placeholders for the tensorflow session.
    
    Arguments:
    n_H0 -- scalar, height of an input image
    n_W0 -- scalar, width of an input image
    n_C0 -- scalar, number of channels of the input
    n_y -- scalar, number of classes
        
    Returns:
    X -- placeholder for the data input, of shape [None, n_H0, n_W0, n_C0] and dtype "float"
    Y -- placeholder for the input labels, of shape [None, n_y] and dtype "float"
    """   
    X = tf.placeholder(tf.float32, [None, n_H0, n_W0, n_C0])
    Y = tf.placeholder(tf.float32, [None, n_y]) 
    return X, Y

def model(X_test, Y_test):
    
    ops.reset_default_graph()# to be able to return the model without overwriting tf variables
    (m, n_H0, n_W0, n_C0) = X_test.shape
    n_y = Y_test.shape[1]
    input_layer = tf.image.resize_image_with_crop_or_pad(X_test, 227, 227)
    
     # Convolutional Layer #1
    conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=96,
        kernel_size=[11, 11],
        strides=4,
        padding="valid",
        activation=tf.nn.relu)

    # Pooling Layer #1
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[3, 3], strides=2)

    # Convolutional Layer #2 and Pooling Layer #2
    conv2 = tf.layers.conv2d(
        inputs=pool1,
        filters=256,
        kernel_size=[5, 5],
        strides=1,
        padding="same",
        activation=tf.nn.relu)

    # Pooling Layer #2
    pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[3, 3], strides=2)

    # Convolutional Layer #3
    conv3 = tf.layers.conv2d(
        inputs=pool2,
        filters=384,
        kernel_size=[3, 3],
        strides=1,
        padding="same",
        activation=tf.nn.relu)

    # Convolutional Layer #4
    conv4 = tf.layers.conv2d(
        inputs=conv3,
        filters=384,
        kernel_size=[3, 3],
        strides=1,
        padding="same",
        activation=tf.nn.relu)

    # Convolutional Layer #5
    conv5 = tf.layers.conv2d(
        inputs=conv4,
        filters=256,
        kernel_size=[3, 3],
        strides=1,
        padding="same",
        activation=tf.nn.relu)

    # Pooling Layer #3
    pool3 = tf.layers.max_pooling2d(inputs=conv5, pool_size=[3, 3], strides=2)

    # Dense Layer
    pool3_flat = tf.reshape(pool3, [-1, 6 * 6 * 256])

    dense1 = tf.layers.dense(inputs=pool3_flat, units=4096,activation=tf.nn.relu)
    dropout1 = tf.layers.dropout(inputs=dense1, rate=0.5, training=False)
    dense2 = tf.layers.dense(inputs=dropout1, units=4096, activation=tf.nn.relu)
     
    saver = tf.train.Saver()
    
    # Create Placeholders of the correct shape
    X, Y = create_placeholders(n_H0, n_W0, n_C0, n_y)
    sess=tf.Session()
    # Restore variables from disk.
    saver.restore(sess, "/Volumes/Users/jianrenw/Desktop/16824_jianrenw_hw1/checkpoints/pascal_model_alexnet/model.ckpt-40000")
    
    # Forward propagation: Build the forward propagation in the tensorflow graph
    
    d2=sess.run([dense2], feed_dict={X:X_test, Y:Y_test})
    
    return d2

def load_pascal(data_dir, split='train'):
    """
    Function to read images from PASCAL data folder.
    Args:
        data_dir (str): Path to the VOC2007 directory.
        split (str): train/val/trainval split to use.
    Returns:
        images (np.ndarray): Return a np.float32 array of
            shape (N, H, W, 3), where H, W are 224px each,
            and each image is in RGB format.
        labels (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are active in that image.
        weights: (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are confidently labeled and 0s for classes that 
            are ambiguous.
    """
    # Wrote this function
    
    namelist=[]
    im={}
    path=os.path.join(data_dir,'ImageSets/Main')
    for filename in os.listdir(path):
        namelist.append(re.split('_|\.',filename))
    for ele in namelist:
        if len(ele)==3 and ele[1]==split:
            tempfile=open(os.path.join(path,''.join(ele[0]+'_'+ele[1]+'.'+ele[2])))
            for line in tempfile:
                line=line.strip('\n')
                tempi=line.split()
                if tempi[0] in im:
                    if tempi[1]=='1':    
                        im[tempi[0]][0][0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        im[tempi[0]][1][0][CLASS_NAMES.index(ele[0])]=1
                else:
                    a=np.zeros((1,20),dtype=np.int32)
                    b=np.zeros((1,20),dtype=np.int32)
                    if tempi[1]=='1':    
                        a[0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        b[0][CLASS_NAMES.index(ele[0])]=1
                    im[tempi[0]]=[a,b]
            tempfile.close()
    
    image_name=list(im.keys())
    N=len(im)
    H=256
    W=256
    
    image=np.zeros((N,H,W,3),dtype=np.float32)
    labels=np.zeros((N,20),dtype=np.int32)
    weights=np.zeros((N,20),dtype=np.int32)
    for i in range(0,N): 
        temp_image_path=os.path.join(data_dir,'JPEGImages/'+image_name[i]+'.jpg')
        tempimage = np.array(ndimage.imread(temp_image_path, flatten=False)) 
        image[i,:,:,:]=scipy.misc.imresize(tempimage, size=(256,256))
        labels[i,:]=im[image_name[i]][0]
        weights[i,:]=im[image_name[i]][1]
    
    return image,labels,weights

def parse_args():
    parser = argparse.ArgumentParser(
        description='Train a classifier in tensorflow!')
    parser.add_argument(
        'data_dir', type=str, default='data/VOC2007',
        help='Path to PASCAL data storage')
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
        
    args = parser.parse_args()
    return args

def _get_el(arr, i):
    try:
        return arr[i]
    except IndexError:
        return arr

data_dir='/Volumes/Users/jianrenw/Desktop/16824_jianrenw_hw1/VOCdevkit/VOC2007'
origin=[]
near=[]
t=[]
color=[]
eval_data, eval_labels, eval_weights = load_pascal(
    data_dir, split='test')

size=eval_data.shape[0]
layer=np.zeros((size,4096))

d2 = model(X_test=eval_data[0:1000,:,:,:], Y_test=eval_labels[0:1000,:])
e=eval_labels[0:1000,:]
color=np.array([[0.1,0.1,0.1],[0.1,0.1,0.5],[0.1,0.1,1],[0.1,0.5,0.1],[0.1,1,0.1],[0.5,0.1,0.1],[1,0.1,0.1],[0.1,0.5,0.5],[0.1,0.5,1],[0.1,1,0.5],[0.1,1,1],[0.5,0.5,0.5],[0.5,1,0.5],[0.5,0.1,0.5],[0.5,1,1],[0.2,0.3,0.7],[0.4,0.7,0.8],[0.6,0.9,1],[0.4,0.4,0.8],[0.9,0.8,0.9]])
colorlist=[]
l=[]

for i in e:
    a=[index for index, value in enumerate(i) if value == 1]
    l.append(np.where(i==1)[0][0])
    temp_color=np.zeros((1,3))
    for i in a:
        temp_color=temp_color+color[a,:]
    temp_color=temp_color/len(a)
    
    colorlist.append(tuple(temp_color[0,:]))

label=list(itemgetter(*l)(CLASS_NAMES))
X_embedded=TSNE(n_components=2, perplexity=40, verbose=2).fit_transform(d2[0])


fig, ax = plt.subplots(figsize=(10, 10))
for i in range(0,1000):
    ax.scatter(X_embedded[i, 0], X_embedded[i, 1],c=colorlist[i],label=label[i],alpha=0.8)

handles, labels = plt.gca().get_legend_handles_labels()
by_label = OrderedDict(zip(labels, handles))
ax.legend(by_label.values(), by_label.keys())

ax.grid(True)
plt.title('tSNE Projection')
plt.show()

    
