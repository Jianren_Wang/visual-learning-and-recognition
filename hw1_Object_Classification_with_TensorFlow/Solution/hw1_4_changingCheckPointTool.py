import tensorflow as tf
import numpy as np

def rename(checkpoint_dir,dry_run=True):
    checkpoint = tf.train.get_checkpoint_state(checkpoint_dir)
    with tf.Session() as sess:
        for var_name, _ in tf.contrib.framework.list_variables(checkpoint_dir):
            # Load the variable
            var = tf.contrib.framework.load_variable(checkpoint_dir, var_name)

            # Set the new name
            new_name = var_name
            if 'biases' in new_name:
                new_name = new_name.replace("biases", "bias")
            if 'fc' in new_name and 'weights' in new_name:
                new_name = new_name.replace("weights", "kernel")
                v=tf.reshape(var, [tf.shape(var)[0]*tf.shape(var)[1]*tf.shape(var)[2],-1])
            elif 'weights' in new_name:
                new_name = new_name.replace("weights", "kernel")

            if dry_run:
                print('%s would be renamed to %s.' % (var_name, new_name))
            else:
                print('Renaming %s to %s.' % (var_name, new_name))
                #Rename the variable
                if 'fc' in new_name and 'kernel' in new_name:
                    #a=np.array2string(sess.run(tf.shape(var)))
                    #b=np.array2string(sess.run(tf.shape(v)))
                    #print('Reshape'+a+'to'+b)
                    var = tf.Variable(v,name=new_name,validate_shape=False)
                else:
                    var = tf.Variable(var, name=new_name)

        if not dry_run:
            # Save the variables
            saver = tf.train.Saver()
            sess.run(tf.global_variables_initializer())
            saver.save(sess, './new_model.ckpt')

# Usage
model_dir = "vgg_16.ckpt"
rename(model_dir,dry_run=False)
