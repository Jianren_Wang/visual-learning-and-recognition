from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import sys
import numpy as np
import tensorflow as tf
import argparse
import os
import re
import scipy
from scipy import ndimage
from PIL import Image
from functools import partial

from eval import compute_map
#import models

tf.logging.set_verbosity(tf.logging.INFO)

CLASS_NAMES = [
    'aeroplane',
    'bicycle',
    'bird',
    'boat',
    'bottle',
    'bus',
    'car',
    'cat',
    'chair',
    'cow',
    'diningtable',
    'dog',
    'horse',
    'motorbike',
    'person',
    'pottedplant',
    'sheep',
    'sofa',
    'train',
    'tvmonitor',
]

def prep_data_augment(image):
    image = tf.random_crop(image, [227, 227, 3])
    image = tf.image.random_flip_left_right(image)
    return image

def cnn_model_fn(features, labels, mode, num_classes=20):
    # Write this function
    """Model function for CNN."""
    # kernel_initializer
    kernel_initializer=tf.random_normal_initializer(mean=0.0,stddev=0.01)
    # bias_initializer
    bias_initializer=tf.zeros_initializer()

    # Input
    input_layer = tf.reshape(features["x"], [-1, 256, 256, 3])
    input_weights = tf.reshape(features["w"], [-1, 20])
    
    # Data Augmentation
    if mode == tf.estimator.ModeKeys.TRAIN:
        input_layer = tf.map_fn(prep_data_augment, input_layer)
    else:
        input_layer = tf.image.resize_image_with_crop_or_pad(input_layer, 227, 227)
        
    # Convolutional Layer #1
    conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=96,
        kernel_size=[11, 11],
        strides=4,
        padding="valid",
        activation=tf.nn.relu,
        kernel_initializer=kernel_initializer,
        bias_initializer=bias_initializer)

    # Pooling Layer #1
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[3, 3], strides=2)

    # Convolutional Layer #2 and Pooling Layer #2
    conv2 = tf.layers.conv2d(
        inputs=pool1,
        filters=256,
        kernel_size=[5, 5],
        strides=1,
        padding="same",
        activation=tf.nn.relu,
        kernel_initializer=kernel_initializer,
        bias_initializer=bias_initializer)
    
    # Pooling Layer #2
    pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[3, 3], strides=2)
    
    # Convolutional Layer #3
    conv3 = tf.layers.conv2d(
        inputs=pool2,
        filters=384,
        kernel_size=[3, 3],
        strides=1,
        padding="same",
        activation=tf.nn.relu,
        kernel_initializer=kernel_initializer,
        bias_initializer=bias_initializer)
    
    # Convolutional Layer #4
    conv4 = tf.layers.conv2d(
        inputs=conv3,
        filters=384,
        kernel_size=[3, 3],
        strides=1,
        padding="same",
        activation=tf.nn.relu,
        kernel_initializer=kernel_initializer,
        bias_initializer=bias_initializer)
    
    # Convolutional Layer #5
    conv5 = tf.layers.conv2d(
        inputs=conv4,
        filters=256,
        kernel_size=[3, 3],
        strides=1,
        padding="same",
        activation=tf.nn.relu,
        kernel_initializer=kernel_initializer,
        bias_initializer=bias_initializer)
    
    # Pooling Layer #3
    pool3 = tf.layers.max_pooling2d(inputs=conv5, pool_size=[3, 3], strides=2)
    
    # Dense Layer
    pool3_flat = tf.reshape(pool3, [-1, 6 * 6 * 256])
    dense1 = tf.layers.dense(inputs=pool3_flat, units=4096,
                            activation=tf.nn.relu,kernel_initializer=kernel_initializer,bias_initializer=bias_initializer)
    dropout1 = tf.layers.dropout(
        inputs=dense1, rate=0.5, training=mode == tf.estimator.ModeKeys.TRAIN)
    dense2 = tf.layers.dense(inputs=dropout1, units=4096,
                            activation=tf.nn.relu,kernel_initializer=kernel_initializer,bias_initializer=bias_initializer)
    dropout2 = tf.layers.dropout(
        inputs=dense2, rate=0.5, training=mode == tf.estimator.ModeKeys.TRAIN)
    
    # Logits Layer
    logits = tf.layers.dense(inputs=dropout2, units=20,kernel_initializer=kernel_initializer,bias_initializer=bias_initializer)

    predictions = {
        "classes": tf.to_int32(logits>0.5),
        "probabilities": tf.nn.sigmoid(logits, name="sigmoid_tensor")
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Calculate Loss (for both TRAIN and EVAL modes)
    loss = tf.identity(tf.losses.sigmoid_cross_entropy(
        multi_class_labels=labels, logits=logits, weights = input_weights), name='loss')
    
    variables_to_restore = tf.contrib.framework.get_trainable_variables()
    scopes = {os.path.dirname(v.name) for v in variables_to_restore }
    tf.train.init_from_checkpoint("/Volumes/Users/jianrenw/Desktop/16824_jianrenw_hw1/checkpoints/pascal_model_alexnet/model.ckpt-40000",{s+'/':s+'/' for s in scopes})
    
    # Configure the Training Op (for TRAIN mode)
    if mode == tf.estimator.ModeKeys.TRAIN:
        #leanrning rate
        learning_rate = tf.train.exponential_decay(0.001, tf.train.get_global_step(),10000, 0.5, staircase=True)
        optimizer = tf.train.MomentumOptimizer(learning_rate=learning_rate,momentum=0.9)
        train_op = optimizer.minimize(
            loss=loss,
            global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(
            mode=mode, loss=loss, train_op=train_op)
    
    
    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(
            labels=labels, predictions=predictions["classes"])}
    return tf.estimator.EstimatorSpec(
        mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)   

def load_pascal(data_dir, split='train'):
    """
    Function to read images from PASCAL data folder.
    Args:
        data_dir (str): Path to the VOC2007 directory.
        split (str): train/val/trainval split to use.
    Returns:
        images (np.ndarray): Return a np.float32 array of
            shape (N, H, W, 3), where H, W are 224px each,
            and each image is in RGB format.
        labels (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are active in that image.
        weights: (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are confidently labeled and 0s for classes that 
            are ambiguous.
    """
    # Wrote this function
    
    namelist=[]
    im={}
    path=os.path.join(data_dir,'ImageSets/Main')
    for filename in os.listdir(path):
        namelist.append(re.split('_|\.',filename))
    for ele in namelist:
        if len(ele)==3 and ele[1]==split:
            tempfile=open(os.path.join(path,''.join(ele[0]+'_'+ele[1]+'.'+ele[2])))
            for line in tempfile:
                line=line.strip('\n')
                tempi=line.split()
                if tempi[0] in im:
                    if tempi[1]=='1':    
                        im[tempi[0]][0][0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        im[tempi[0]][1][0][CLASS_NAMES.index(ele[0])]=1
                else:
                    a=np.zeros((1,20),dtype=np.int32)
                    b=np.zeros((1,20),dtype=np.int32)
                    if tempi[1]=='1':    
                        a[0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        b[0][CLASS_NAMES.index(ele[0])]=1
                    im[tempi[0]]=[a,b]
            tempfile.close()
    
    image_name=list(im.keys())
    N=len(im)
    H=256
    W=256
    
    image=np.zeros((N,H,W,3),dtype=np.float32)
    labels=np.zeros((N,20),dtype=np.int32)
    weights=np.zeros((N,20),dtype=np.int32)
    for i in range(0,N): 
        temp_image_path=os.path.join(data_dir,'JPEGImages/'+image_name[i]+'.jpg')
        tempimage = np.array(ndimage.imread(temp_image_path, flatten=False)) 
        image[i,:,:,:]=scipy.misc.imresize(tempimage, size=(256,256))
        labels[i,:]=im[image_name[i]][0]
        weights[i,:]=im[image_name[i]][1]
    
    return image,labels,weights

def parse_args():
    parser = argparse.ArgumentParser(
        description='Train a classifier in tensorflow!')
    parser.add_argument(
        'data_dir', type=str, default='data/VOC2007',
        help='Path to PASCAL data storage')
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    return args

def _get_el(arr, i):
    try:
        return arr[i]
    except IndexError:
        return arr

def main():
    args = parse_args()
    #data_dir='/Volumes/Users/jianrenw/Documents/VOCdevkit/VOC2007'
    # Load training and eval data
    train_data, train_labels, train_weights = load_pascal(
        args.data_dir, split='trainval')
    eval_data, eval_labels, eval_weights = load_pascal(
        args.data_dir, split='test')

    pascal_classifier = tf.estimator.Estimator(
        model_fn=partial(cnn_model_fn,
                         num_classes=train_labels.shape[1]),
        model_dir="/Volumes/Users/jianrenw/Desktop/16824_jianrenw_hw1/checkpoints/pascal_model_alexnet")
    # Evaluate the model and print results
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": eval_data, "w": eval_weights},
        y=eval_labels,
        num_epochs=1,
        shuffle=False)
    pascal_classifier.evaluate(input_fn=eval_input_fn)
    pred = list(pascal_classifier.predict(input_fn=eval_input_fn))
    pred = np.stack([p['probabilities'] for p in pred])
    AP = compute_map(eval_labels, pred, eval_weights, average=None)
    print('Obtained {} mAP'.format(np.mean(AP)))
    print('per class:')
    for cid, cname in enumerate(CLASS_NAMES):
        print('{}: {}'.format(cname, _get_el(AP, cid)))


if __name__ == "__main__":
    main()
