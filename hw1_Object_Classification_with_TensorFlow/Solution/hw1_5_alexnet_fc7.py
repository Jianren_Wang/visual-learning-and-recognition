from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import sys
import numpy as np
import tensorflow as tf
import argparse
import os
import re
import scipy
from scipy import ndimage
import scipy.misc
from PIL import Image
from functools import partial
from tensorflow.python.framework import ops
from sklearn.neighbors import NearestNeighbors
from eval import compute_map
import random

tf.logging.set_verbosity(tf.logging.INFO)

CLASS_NAMES = [
    'aeroplane',
    'bicycle',
    'bird',
    'boat',
    'bottle',
    'bus',
    'car',
    'cat',
    'chair',
    'cow',
    'diningtable',
    'dog',
    'horse',
    'motorbike',
    'person',
    'pottedplant',
    'sheep',
    'sofa',
    'train',
    'tvmonitor',
]

def create_placeholders(n_H0, n_W0, n_C0, n_y):
    """
    Creates the placeholders for the tensorflow session.
    
    Arguments:
    n_H0 -- scalar, height of an input image
    n_W0 -- scalar, width of an input image
    n_C0 -- scalar, number of channels of the input
    n_y -- scalar, number of classes
        
    Returns:
    X -- placeholder for the data input, of shape [None, n_H0, n_W0, n_C0] and dtype "float"
    Y -- placeholder for the input labels, of shape [None, n_y] and dtype "float"
    """   
    X = tf.placeholder(tf.float32, [None, n_H0, n_W0, n_C0])
    Y = tf.placeholder(tf.float32, [None, n_y]) 
    return X, Y

def model(X_test, Y_test):
    
    ops.reset_default_graph()# to be able to return the model without overwriting tf variables
    (m, n_H0, n_W0, n_C0) = X_test.shape
    n_y = Y_test.shape[1]
    input_layer = tf.image.resize_image_with_crop_or_pad(X_test, 227, 227)
    
     # Convolutional Layer #1
    conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=96,
        kernel_size=[11, 11],
        strides=4,
        padding="valid",
        activation=tf.nn.relu)

    # Pooling Layer #1
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[3, 3], strides=2)

    # Convolutional Layer #2 and Pooling Layer #2
    conv2 = tf.layers.conv2d(
        inputs=pool1,
        filters=256,
        kernel_size=[5, 5],
        strides=1,
        padding="same",
        activation=tf.nn.relu)

    # Pooling Layer #2
    pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[3, 3], strides=2)

    # Convolutional Layer #3
    conv3 = tf.layers.conv2d(
        inputs=pool2,
        filters=384,
        kernel_size=[3, 3],
        strides=1,
        padding="same",
        activation=tf.nn.relu)

    # Convolutional Layer #4
    conv4 = tf.layers.conv2d(
        inputs=conv3,
        filters=384,
        kernel_size=[3, 3],
        strides=1,
        padding="same",
        activation=tf.nn.relu)

    # Convolutional Layer #5
    conv5 = tf.layers.conv2d(
        inputs=conv4,
        filters=256,
        kernel_size=[3, 3],
        strides=1,
        padding="same",
        activation=tf.nn.relu)

    # Pooling Layer #3
    pool3 = tf.layers.max_pooling2d(inputs=conv5, pool_size=[3, 3], strides=2)

    # Dense Layer
    pool3_flat = tf.reshape(pool3, [-1, 6 * 6 * 256])

    dense1 = tf.layers.dense(inputs=pool3_flat, units=4096,activation=tf.nn.relu)
    dropout1 = tf.layers.dropout(inputs=dense1, rate=0.5, training=False)
    dense2 = tf.layers.dense(inputs=dropout1, units=4096, activation=tf.nn.relu)
     
    saver = tf.train.Saver()
    
    # Create Placeholders of the correct shape
    X, Y = create_placeholders(n_H0, n_W0, n_C0, n_y)
    sess=tf.Session()
    # Restore variables from disk.
    saver.restore(sess, "hw1_summary/pascal_model_alexnet/model.ckpt-40000")
    
    # Forward propagation: Build the forward propagation in the tensorflow graph
    
    d2=sess.run([dense2], feed_dict={X:X_test, Y:Y_test})
    
    return d2

def load_pascal(data_dir, split='train'):
    """
    Function to read images from PASCAL data folder.
    Args:
        data_dir (str): Path to the VOC2007 directory.
        split (str): train/val/trainval split to use.
    Returns:
        images (np.ndarray): Return a np.float32 array of
            shape (N, H, W, 3), where H, W are 224px each,
            and each image is in RGB format.
        labels (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are active in that image.
        weights: (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are confidently labeled and 0s for classes that 
            are ambiguous.
    """
    # Wrote this function
    
    namelist=[]
    im={}
    path=os.path.join(data_dir,'ImageSets/Main')
    for filename in os.listdir(path):
        namelist.append(re.split('_|\.',filename))
    for ele in namelist:
        if len(ele)==3 and ele[1]==split:
            tempfile=open(os.path.join(path,''.join(ele[0]+'_'+ele[1]+'.'+ele[2])))
            for line in tempfile:
                line=line.strip('\n')
                tempi=line.split()
                if tempi[0] in im:
                    if tempi[1]=='1':    
                        im[tempi[0]][0][0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        im[tempi[0]][1][0][CLASS_NAMES.index(ele[0])]=1
                else:
                    a=np.zeros((1,20),dtype=np.int32)
                    b=np.zeros((1,20),dtype=np.int32)
                    if tempi[1]=='1':    
                        a[0][CLASS_NAMES.index(ele[0])]=1
                    if tempi[1]!='0':
                        b[0][CLASS_NAMES.index(ele[0])]=1
                    im[tempi[0]]=[a,b]
            tempfile.close()
    
    image_name=list(im.keys())
    N=len(im)
    H=256
    W=256
    
    image=np.zeros((N,H,W,3),dtype=np.float32)
    labels=np.zeros((N,20),dtype=np.int32)
    weights=np.zeros((N,20),dtype=np.int32)
    for i in range(0,N): 
        temp_image_path=os.path.join(data_dir,'JPEGImages/'+image_name[i]+'.jpg')
        tempimage = np.array(ndimage.imread(temp_image_path, flatten=False)) 
        image[i,:,:,:]=scipy.misc.imresize(tempimage, size=(256,256))
        labels[i,:]=im[image_name[i]][0]
        weights[i,:]=im[image_name[i]][1]
    
    return image,labels,weights

def parse_args():
    parser = argparse.ArgumentParser(
        description='Train a classifier in tensorflow!')
    parser.add_argument(
        'data_dir', type=str, default='data/VOC2007',
        help='Path to PASCAL data storage')
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    return args

def _get_el(arr, i):
    try:
        return arr[i]
    except IndexError:
        return arr

data_dir='VOCdevkit/VOC2007/'
origin=[]
near=[]
t=[]
eval_data, eval_labels, eval_weights = load_pascal(
    data_dir, split='test')

size=eval_data.shape[0]
layer=np.zeros((size,4096))
k=int(size/100)
m=size%100
for i in range(0,k):
    d2 = model(X_test=eval_data[i*100:i*100+100,:,:,:], Y_test=eval_labels[i*100:i*100+100,:])
    d2 = d2[0]
    layer[i*100:i*100+100,:] = d2

d2 = model(X_test=eval_data[size-m:size,:,:,:], Y_test=eval_labels[size-m:size,:])
d2 = d2[0]
layer[size-m:size,:] = d2

for i in range(0,20):
    for j in range(0,size):
        if eval_labels[j,:][i]==1:
            if j not in t: 
                t.append(j)
                break
            
for i in t:
    test=layer[i, :]
    samples=np.delete(layer,i,axis=0)
    neigh = NearestNeighbors(n_neighbors=1)
    neigh.fit(samples) 
    nearest=neigh.kneighbors(test.reshape(1,-1))[1][0][0]
    if nearest>=i:
        nearest=nearest+1
    near.append(nearest)

print(t)
print(near)

for i in t:
    scipy.misc.imsave(str(i)+'origin.jpg', eval_data[i,:,:,:])
for i in near:
    scipy.misc.imsave(str(i)+'near.jpg', eval_data[i,:,:,:])


    
