## Answers

Q.No. | Answer
------|-------
Q 0.1 | 0.9687, see [`00_mnist_sol.py`](00_mnist_sol.py).
Q 0.2 | 0.9765
Q 0.3 | Validation accuracy: ![](results/Q0.3.png) <br/> Similarly also show training loss.
Q 1.1-1.3 | I don't provide solution here, but it should be trivial to write, after looking at the solutions for Q2 and 3.
Q 2.1-2.3 | Look for implementation in [`02_pascal_sol.py`](02_pascal_sol.py). Set `MODEL_NAME='alexnet'` in the file, and run using `python 02_pascal_sol.py $DATA_DIR`. I was able to get around 45% mAP, without specialized learning rates for biases, multi-crop testing etc. Detailed results in A1 below.
Q 3.1 | Change `MODEL_NAME=vgg16` in the `02_pascal_sol.py`, and set the `LOAD_MODEL_PATH=''`, to not load the pre-trained model. It might not converge because [VGG models need to be trained in stages](https://groups.google.com/forum/#!topic/caffe-users/zxg6vPsIzmQ), unless using specialized initialization like MSRA [He *et al.*, ICCV'15], or using batch norms.
Q 3.2 | Implemented in the provided solution (`02_pascal_sol.py`). It stores the tensorboard summaries into the `$MODEL_DIR`.
Q 4.1 | Use `02_pascal_sol.py`, set `LOAD_MODEL_PATH` to the location of VGG pretrained model. I get accuracy of around 80% mAP, details in A2.
Q 5.1 | Extract and visualize the weights from intermediate checkpoint files.
Q 5.2 | Use a similar technique to extract intermediate features, as I use to extract the final predictions (`sigmoid_tensor`).
Q 5.3 | Similar to 5.2, compute tSNE and plot.
Q 5.4 | Ideally look for the incorrectly classified images from the hard classes to figure why those are hard. Classes like cats get better with ImageNet init, as ImageNet has a lot of cats, hence filters are well tuned to detect them.
Q 6 | Extra credit!



## Appendix
### A1. Per-class accuracy for alexnet (from scratch)
```txt
Random AP: 0.074145462102 mAP
GT AP: 1.0 mAP
Obtained 0.45279351935 mAP
per class:
aeroplane: 0.659213101869
bicycle: 0.444965974276
bird: 0.405085694359
boat: 0.508038722429
bottle: 0.174917283238
bus: 0.409979079939
car: 0.67706587155
cat: 0.385728006075
chair: 0.420720944755
cow: 0.228726722541
diningtable: 0.352571735411
dog: 0.343152272707
horse: 0.692302655217
motorbike: 0.586609643526
person: 0.81576741107
pottedplant: 0.240073206917
sheep: 0.305781699472
sofa: 0.355041770256
train: 0.661054556309
tvmonitor: 0.389074035089
```

### A2. Per-class accuracy for VGG-16 (from ImageNet)
```txt
Random AP: 0.0737947698455 mAP
GT AP: 1.0 mAP
Obtained 0.807364454371 mAP
per class:
aeroplane: 0.948554992058
bicycle: 0.885200949518
bird: 0.919046885403
boat: 0.894019880967
bottle: 0.499036981214
bus: 0.793068475918
car: 0.897871699439
cat: 0.911636678527
chair: 0.647685926469
cow: 0.654927373755
diningtable: 0.757355341645
dog: 0.89980289567
horse: 0.899459744084
motorbike: 0.854265800373
person: 0.954273261759
pottedplant: 0.599524753206
sheep: 0.781027209245
sofa: 0.679789803491
train: 0.932651540963
tvmonitor: 0.738088893712
```
