from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import sys
import numpy as np
import tensorflow as tf
import argparse
import os.path as osp
from PIL import Image
from functools import partial

from eval import compute_map
import models

tf.logging.set_verbosity(tf.logging.INFO)

CLASS_NAMES = [
    'aeroplane',
    'bicycle',
    'bird',
    'boat',
    'bottle',
    'bus',
    'car',
    'cat',
    'chair',
    'cow',
    'diningtable',
    'dog',
    'horse',
    'motorbike',
    'person',
    'pottedplant',
    'sheep',
    'sofa',
    'train',
    'tvmonitor',
]
IMG_SIZE = 256
CROP_SIZE = 227
# As per Krahenbuhl et al. ICLR'16
# https://github.com/philkr/voc-classification/blob/master/src/train_cls.py
BATCH_SIZE = 16  # paper says 10, but code uses 16
GRAD_CLIP = 10.0
# LR parameters
GAMMA = 0.5
STD_IMG = False
MODEL_NAME = 'alexnet'  # alexnet/vgg16
# MODEL_NAME = 'vgg16'
if MODEL_NAME == 'alexnet':
    LOAD_MODEL_PATH = ''
    NUM_ITERS = 80000
    STEP_SIZE = 10000
    INIT_LR = 0.001
    MODEL_DIR = '/tmp/alexnet_scratch/'
elif MODEL_NAME == 'vgg16':
    LOAD_MODEL_PATH = 'vgg_16.ckpt'
    NUM_ITERS = 4000
    STEP_SIZE = 1000
    INIT_LR = 0.0001
    MODEL_DIR = '/tmp/vgg16_imagenet/'


def cnn_model_fn(features, labels, mode, num_classes=20):
    """Model function for CNN."""
    original_images = features["x"]
    label_weights = features['w']

    input_layer = tf.to_float(original_images)
    is_training = (mode == tf.estimator.ModeKeys.TRAIN)

    if is_training:
        input_layer = tf.random_crop(
            input_layer, [BATCH_SIZE, CROP_SIZE, CROP_SIZE, 3])
        input_layer = tf.stack([
            tf.image.random_flip_left_right(el) for el in
            tf.unstack(input_layer)])
    else:
        input_layer = tf.image.resize_image_with_crop_or_pad(
            input_layer, CROP_SIZE, CROP_SIZE)

    # standardize image
    input_layer -= np.array([123.675, 116.28, 103.53])[
        np.newaxis, np.newaxis, np.newaxis, :]  # RGB mean
    if STD_IMG:
        input_layer /= np.array([57.375, 57.12, 58.39])[
            np.newaxis, np.newaxis, np.newaxis, :]  # RGB std

    logits = models.get_model(MODEL_NAME, input_layer,
                              num_classes=num_classes,
                              is_training=is_training)

    predictions = {
        "probabilities": tf.sigmoid(logits, name="sigmoid_tensor")
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Calculate Loss (for both TRAIN and EVAL modes)
    # Hacky way to do weighted loss
    labels *= label_weights
    logits *= tf.to_float(label_weights)
    loss = tf.identity(tf.losses.sigmoid_cross_entropy(
        multi_class_labels=labels, logits=logits),
        name='loss')

    # Configure the Training Op (for TRAIN mode)
    if is_training:
        no_op = tf.no_op()
        if len(LOAD_MODEL_PATH) > 0:
            init_fn = tf.contrib.slim.assign_from_checkpoint_fn(
                LOAD_MODEL_PATH,
                [el for el in tf.trainable_variables() if
                 'fc8' not in el.name],
                ignore_missing_vars=True)
        else:
            def init_fn(sess):
                return no_op
        learning_rate = tf.train.exponential_decay(
            INIT_LR, tf.train.get_global_step(), STEP_SIZE, GAMMA,
            staircase=True)
        opt = tf.train.MomentumOptimizer(
            learning_rate=learning_rate, momentum=0.9)

        grads_and_vars = opt.compute_gradients(loss, tf.trainable_variables())
        grads_and_vars = [
            (tf.clip_by_norm(gv[0], GRAD_CLIP), gv[1])
            for gv in grads_and_vars]
        train_op = opt.apply_gradients(grads_and_vars,
                                       global_step=tf.train.get_global_step())

        # Add summaries
        tf.summary.image("images", original_images)
        for grad, var in grads_and_vars:
            tf.summary.histogram(var.name, grad, family="gradients")

        load_hook = tf.train.SessionRunHook()

        def run_after_create(sess, coord):
            return init_fn(sess)
        load_hook.after_create_session = run_after_create
        return tf.estimator.EstimatorSpec(
            mode=mode, loss=loss, train_op=train_op,
            training_hooks=[load_hook])

    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
        "AUC": tf.metrics.auc(
            labels=labels, predictions=predictions["probabilities"],
            curve='PR')}
    return tf.estimator.EstimatorSpec(
        mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)


def load_pascal(data_dir, split='train'):
    """
    Function to read images from PASCAL data folder.
    Args:
        data_dir (str): Path to the VOC2007 directory.
        split (str): train/val/trainval split to use.
    Returns:
        images (np.ndarray): Return a np.float32 array of
            shape (N, H, W, 3), where H, W are 224px each,
            and each image is in RGB format.
        labels (np.ndarray): An array of shape (N, 20) of
            type np.int32, with 0s and 1s; 1s for classes that
            are active in that image.
    """
    imid_to_labels = {}
    imid_to_weights = {}
    for cid, cname in enumerate(CLASS_NAMES):
        with open(osp.join(
                data_dir, 'ImageSets/Main/',
                cname + '_{}.txt'.format(split)), 'r') as f:
            for line in f:
                imid, active = line.split()
                active = int(active)
                if active >= 0:
                    # add to the labels for this image
                    if imid not in imid_to_labels:
                        imid_to_labels[imid] = np.zeros(
                            (len(CLASS_NAMES),), dtype=np.int32)
                        imid_to_weights[imid] = np.ones(
                            (len(CLASS_NAMES),), dtype=np.int32)
                    if active == 0:
                        imid_to_weights[imid][cid] = 0
                    imid_to_labels[imid][cid] = 1
    # Now read the original images
    all_images = []
    all_labels = []
    all_weights = []
    for imid, label in imid_to_labels.items():
        all_labels.append(label)
        all_weights.append(imid_to_weights[imid])
        im = Image.open(osp.join(data_dir, 'JPEGImages',
                                 '{}.jpg'.format(imid)), 'r')
        # for simplicity, resize to 256x256
        im = im.resize((IMG_SIZE, IMG_SIZE))
        all_images.append(np.array(im))
    images = np.stack(all_images)
    labels = np.stack(all_labels)
    weights = np.stack(all_weights)
    print('Loaded {:d} images for {:s} split, with {:.3f} avg '
          'active labels per img'.format(
              len(all_images), split, labels.sum() / labels.shape[0]))
    return images.astype(np.float32), labels, weights


def parse_args():
    parser = argparse.ArgumentParser(
        description='Train a classifier in tensorflow!')
    parser.add_argument(
        'data_dir', type=str, default='data/VOC2007',
        help='Path to PASCAL data storage')
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    return args


def _get_el(arr, i):
    try:
        return arr[i]
    except IndexError:
        return arr


def main():
    args = parse_args()
    # Load training and eval data
    train_data, train_labels, train_weights = load_pascal(
        args.data_dir, split='trainval')
    eval_data, eval_labels, eval_weights = load_pascal(
        args.data_dir, split='test')

    pascal_classifier = tf.estimator.Estimator(
        model_fn=partial(cnn_model_fn,
                         num_classes=train_labels.shape[1]),
        model_dir=MODEL_DIR)
    tensors_to_log = {"loss": "loss"}
    logging_hook = tf.train.LoggingTensorHook(
        tensors=tensors_to_log, every_n_iter=10)
    summary_hook = tf.train.SummarySaverHook(
        save_secs=30,
        output_dir=MODEL_DIR,
        scaffold=tf.train.Scaffold(summary_op=tf.summary.merge_all()))
    # Train the model
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": train_data, "w": train_weights},
        y=train_labels,
        batch_size=BATCH_SIZE,
        num_epochs=None,
        shuffle=True)
    pascal_classifier.train(
        input_fn=train_input_fn,
        steps=NUM_ITERS,
        hooks=[logging_hook, summary_hook])
    # Evaluate the model and print results
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": eval_data, "w": eval_weights},
        y=eval_labels,
        num_epochs=1,
        shuffle=False)
    pred = list(pascal_classifier.predict(input_fn=eval_input_fn))
    pred = np.stack([p['probabilities'] for p in pred])
    rand_AP = compute_map(
        eval_labels, np.random.random(eval_labels.shape),
        eval_weights, average=None)
    print('Random AP: {} mAP'.format(np.mean(rand_AP)))
    gt_AP = compute_map(
        eval_labels, eval_labels, eval_weights, average=None)
    print('GT AP: {} mAP'.format(np.mean(gt_AP)))
    AP = compute_map(eval_labels, pred, eval_weights, average=None)
    print('Obtained {} mAP'.format(np.mean(AP)))
    print('per class:')
    for cid, cname in enumerate(CLASS_NAMES):
        print('{}: {}'.format(cname, _get_el(AP, cid)))


if __name__ == "__main__":
    main()
