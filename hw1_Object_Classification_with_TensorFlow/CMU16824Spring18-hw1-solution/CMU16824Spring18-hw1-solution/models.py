import tensorflow as tf
slim = tf.contrib.slim


def alexnet(input_layer, num_classes=1000, is_training=True):
    # I am ignoring some other things, like grouped convolutions,
    # using 2x learning rate for biases etc.
    with tf.variable_scope('caffenet'):
        with slim.arg_scope(
                [slim.conv2d, slim.fully_connected],
                weights_initializer=tf.random_normal_initializer(
                    0, 0.01),
                weights_regularizer=tf.contrib.layers.l2_regularizer(
                    1e-6),
                # The non zero bias is really important
                biases_initializer=tf.constant_initializer(0.1)):
            net = slim.conv2d(input_layer, 96, [11, 11], stride=4,
                              scope='conv1')
            net = slim.max_pool2d(net, [3, 3], stride=2, scope='pool1')
            # net = slim.nn.lrn(net, depth_radius=5, alpha=0.0001, beta=0.75)
            net = slim.conv2d(net, 256, [5, 5], padding='SAME',  scope='conv2')
            net = slim.max_pool2d(net, [3, 3], stride=2, scope='pool2')
            # net = slim.nn.lrn(net, depth_radius=5, alpha=0.0001, beta=0.75)
            net = slim.conv2d(net, 384, [3, 3], padding='SAME', scope='conv3')
            net = slim.conv2d(net, 384, [3, 3], padding='SAME',
                              biases_initializer=tf.ones_initializer(),
                              scope='conv4')
            net = slim.conv2d(net, 256, [3, 3], padding='SAME',
                              biases_initializer=tf.ones_initializer(),
                              scope='conv5')
            net = slim.max_pool2d(net, [3, 3], stride=2, scope='pool5')

            net = tf.contrib.layers.flatten(net)
            net = slim.fully_connected(
                net, 4096, scope='fc6')
            net = slim.dropout(
                net, 0.5,
                is_training=is_training,
                scope='dropout6')
            net = slim.fully_connected(
                net, 4096, scope='fc7')
            net = slim.dropout(
                net, 0.5,
                is_training=is_training,
                scope='dropout7')
            logits = slim.fully_connected(
                net, num_classes, activation_fn=None, scope='fc8')
    return logits


def vgg16(input_layer, num_classes=1000, is_training=True):
    with tf.variable_scope('vgg_16'):
        with slim.arg_scope(
                [slim.conv2d, slim.fully_connected],
                weights_initializer=tf.random_normal_initializer(0, 0.01),
                weights_regularizer=tf.contrib.layers.l2_regularizer(0.0001),
                biases_initializer=tf.zeros_initializer()):
            net = slim.repeat(input_layer, 2, slim.conv2d, 64, [3, 3],
                              scope='conv1')
            net = slim.max_pool2d(net, [2, 2], scope='pool1')
            net = slim.repeat(net, 2, slim.conv2d, 128, [3, 3], scope='conv2')
            net = slim.max_pool2d(net, [2, 2], scope='pool2')
            net = slim.repeat(net, 3, slim.conv2d, 256, [3, 3], scope='conv3')
            net = slim.max_pool2d(net, [2, 2], scope='pool3')
            net = slim.repeat(net, 3, slim.conv2d, 512, [3, 3], scope='conv4')
            net = slim.max_pool2d(net, [2, 2], scope='pool4')
            net = slim.repeat(net, 3, slim.conv2d, 512, [3, 3], scope='conv5')
            net = slim.max_pool2d(net, [2, 2], scope='pool5')

            # Use conv2d instead of fully_connected layers.
            net = slim.conv2d(net, 4096, [7, 7], padding='same',
                              scope='fc6')
            net = slim.dropout(
                net, 0.5,
                is_training=is_training,
                scope='dropout6')
            net = slim.conv2d(net, 4096, [1, 1], scope='fc7')
            net = tf.reduce_mean(net, [1, 2], keep_dims=True,
                                 name='global_pool')
            net = slim.dropout(
                net, 0.8,
                is_training=is_training,
                scope='dropout7')
            net = slim.conv2d(net, num_classes, [1, 1],
                              activation_fn=None,
                              normalizer_fn=None,
                              scope='fc8')
            logits = tf.squeeze(net, [1, 2], name='fc8/squeezed')
    return logits


def get_model(model_name, *args, **kwargs):
    if model_name == 'alexnet':
        return alexnet(*args, **kwargs)
    elif model_name == 'vgg16':
        return vgg16(*args, **kwargs)
    else:
        raise NotImplementedError()
