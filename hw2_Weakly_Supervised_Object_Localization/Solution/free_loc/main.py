import argparse
import os
import shutil
import time
import sys
sys.path.insert(0,'/home/ubuntu/hw2/hw2-release/code/faster_rcnn')
sys.path.insert(0,'/home/ubuntu/hw2/hw2-release/code')
import sklearn
import sklearn.metrics

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.nn.functional as F
import torch.backends.cudnn as cudnn
import torch.distributed as dist
import torch.optim
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models
import visdom

from datasets.factory import get_imdb
from custom import *

import _init_paths
import logger

import numpy as np
import cv2
import matplotlib
import matplotlib.pyplot as plt

model_names = sorted(name for name in models.__dict__
    if name.islower() and not name.startswith("__")
    and callable(models.__dict__[name]))

parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')
parser.add_argument('--arch', default='localizer_alexnet_robust')
parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
                    help='number of data loading workers (default: 4)')
parser.add_argument('--epochs', default=2, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                    help='manual epoch number (useful on restarts)')
parser.add_argument('-b', '--batch_size', default=32, type=int,
                    metavar='N', help='mini-batch size (default: 256)')
parser.add_argument('--lr', '--learning-rate', default=0.01, type=float,
                    metavar='LR', help='initial learning rate')
parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                    help='momentum')
parser.add_argument('--weight-decay', '--wd', default=1e-4, type=float,
                    metavar='W', help='weight decay (default: 1e-4)')
parser.add_argument('--print-freq', '-p', default=10, type=int,
                    metavar='N', help='print frequency (default: 10)')
parser.add_argument('--eval-freq', default=10, type=int,
                    metavar='N', help='print frequency (default: 10)')
parser.add_argument('--resume', default='', type=str, metavar='PATH',
                    help='path to latest checkpoint (default: none)')
parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                    help='evaluate model on validation set')
parser.add_argument('--pretrained', dest='pretrained', action='store_true',
                    help='use pre-trained model')
parser.add_argument('--world-size', default=1, type=int,
                    help='number of distributed processes')
parser.add_argument('--dist-url', default='tcp://224.66.41.62:23456', type=str,
                    help='url used to set up distributed training')
parser.add_argument('--dist-backend', default='gloo', type=str,
                    help='distributed backend')
parser.add_argument('--vis',action='store_true')

best_prec1 = 0


def main():
    global args, best_prec1
    args = parser.parse_args()
    args.distributed = args.world_size > 1

    # create model
    print("=> creating model '{}'".format(args.arch))
    if args.arch=='localizer_alexnet':
        model = localizer_alexnet(pretrained=args.pretrained)
    elif args.arch=='localizer_alexnet_robust':
        model = localizer_alexnet_robust(pretrained=args.pretrained)
    print(model)

    model.features = torch.nn.DataParallel(model.features)
    model.cuda()

    # TODO:
    # define loss function (criterion) and optimizer
    criterion = nn.BCELoss().cuda()
    optimizer = torch.optim.SGD(model.parameters(), args.lr,
                                momentum=args.momentum,
                                weight_decay=args.weight_decay)

    # optionally resume from a checkpoint
    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            args.start_epoch = checkpoint['epoch']
            best_prec1 = checkpoint['best_prec1']
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))

    cudnn.benchmark = True

    # set random seed
    torch.manual_seed(seed=1)
    np.random.seed(seed=1)

    # Data loading code
    # TODO: Write code for IMDBDataset in custom.py
    trainval_imdb = get_imdb('voc_2007_trainval')
    test_imdb = get_imdb('voc_2007_test')

    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])
    train_dataset = IMDBDataset(
        trainval_imdb,
        transforms.Compose([
            transforms.Resize((512,512)),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            normalize,
        ]))
    train_sampler = None
    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=args.batch_size, shuffle=(train_sampler is None),
        num_workers=args.workers, pin_memory=True, sampler=train_sampler)

    val_loader = torch.utils.data.DataLoader(
        IMDBDataset(test_imdb, transforms.Compose([
            transforms.Resize((512,512)),
            transforms.ToTensor(),
            normalize,
        ])),
        batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True)

    if args.evaluate:
        validate(val_loader, model, criterion)
        return

    # TODO: Create loggers for visdom and tboard
    # TODO: You can pass the logger objects to train(), make appropriate
    # modifications to train()
    if args.arch == 'localizer_alexnet':
        args.epochs = 30
        #args.epochs = 2
        my_logger = logger.Logger('./logs1_5/alexnet', name='freeloc_alexnet')
        vis = visdom.Visdom(server='http://localhost', port='8097')
    elif args.arch=='localizer_alexnet_robust':
        args.epochs = 45
        my_logger = logger.Logger('./logs1_6/alexnet_robust', name='freeloc_alexnet_robust')
        vis = visdom.Visdom(server='http://localhost', port='8098')


    for epoch in range(args.start_epoch, args.epochs):
        adjust_learning_rate(optimizer, epoch)

        # train for one epoch
        step = train(train_loader, model, criterion, optimizer, epoch, my_logger, vis)
        
        # evaluate on validation set
        #if epoch%args.eval_freq==0 or epoch==args.epochs-1:
        if epoch%2==0:
            m1, m2 = validate(val_loader, model, criterion, epoch, my_logger, vis)
            score = m1*m2
            # remember best prec@1 and save checkpoint
            is_best =  score > best_prec1
            best_prec1 = max(score, best_prec1)
            save_checkpoint({
                'epoch': epoch + 1,
                'arch': args.arch,
                'state_dict': model.state_dict(),
                'best_prec1': best_prec1,
                'optimizer' : optimizer.state_dict(),
            }, is_best)
            my_logger.scalar_summary(tag='validate/metric1', value=m1, step=step)
            my_logger.scalar_summary(tag='validate/metric2', value=m2, step=step)


#TODO: You can add input arguments if you wish
def train(train_loader, model, criterion, optimizer, epoch, my_logger, vis):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    avg_m1 = AverageMeter()
    avg_m2 = AverageMeter()

    # switch to train mode
    model.train()

    end = time.time()
    for i, (input, target) in enumerate(train_loader):
        step = epoch * len(train_loader) + i
        # measure data loading time
        data_time.update(time.time() - end)

        target = target.type(torch.FloatTensor).cuda(async=True)
        input_var = torch.autograd.Variable(input, requires_grad=True)
        target_var = torch.autograd.Variable(target)

        # TODO: Get output from model
        # TODO: Perform any necessary functions on the output
        # TODO: Compute loss using ``criterion``
        # compute output
        feature=model(input_var)
        maxpooling = nn.MaxPool2d(kernel_size=(feature.size()[2], feature.size()[3]))
        output = maxpooling(feature)
        output = output.view(output.shape[0], 20)
        imoutput = F.sigmoid(output)
        loss=criterion(imoutput,target_var)

        # measure metrics and record loss
        m1 = metric1(imoutput.data, target)
        m2 = metric2(imoutput.data, target)
        losses.update(loss.data[0], input.size(0))
        avg_m1.update(m1[0], input.size(0))
        avg_m2.update(m2[0], input.size(0))
        
        # TODO: 
        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()


        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()
        
        if i % args.print_freq == 0:
            print('Epoch: [{0}][{1}/{2}]\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'Data {data_time.val:.3f} ({data_time.avg:.3f})\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                  'Metric1 {avg_m1.val:.3f} ({avg_m1.avg:.3f})\t'
                  'Metric2 {avg_m2.val:.3f} ({avg_m2.avg:.3f})'.format(
                   epoch, i, len(train_loader), batch_time=batch_time,
                   data_time=data_time, loss=losses, avg_m1=avg_m1,
                   avg_m2=avg_m2))

            #TODO: Visualize things as mentioned in handout
            #TODO: Visualize at appropriate intervals
            my_logger.scalar_summary(tag='train/loss', value=losses.avg, step=step)
            my_logger.scalar_summary(tag='train/metric1', value=avg_m1.avg, step=step)
            my_logger.scalar_summary(tag='train/metric2', value=avg_m2.avg, step=step)
            my_logger.model_param_histo_summary(model=model, step=step)

        mean=[0.485, 0.456, 0.406]
        std=[0.229, 0.224, 0.225]
        classes = train_loader.dataset.classes
        if (epoch%10==0 or epoch==args.epochs-1) and i%int(len(train_loader)/4)==0:
            batch_size=feature.shape[0]
            for j in range(batch_size):
                k=int(np.nonzero(target[j])[0])
                img_raw=input.cpu().numpy()[j]
                f=feature.data.cpu().numpy()[j]
                #Denormalize
                for c in range(3):
                    img_raw[c] = img_raw[c]*std[c]+mean[c]

                img_raw_t = np.transpose(img_raw, (1, 2, 0))
                img = np.zeros((1, 512, 512, 3))
                img[0] = img_raw_t
                img_tag = str(epoch)+'_'+str(i)+'_'+str(j)+'_image'
                my_logger.image_summary(tag='/train/'+img_tag, images=img, step=step)
                vis.image(img_raw, opts=dict(title=img_tag)) 

                out=imoutput.data.cpu().numpy()[j]
                out_idx=np.where(out==max(out))[0][0]

                heat_raw=f[out_idx]
                heat_raw = (heat_raw - heat_raw.min()) / (heat_raw.max() - heat_raw.min())
                heatmap=np.zeros((1, 512, 512, 4))
                img_heatmap = np.zeros((1, heat_raw.shape[0], heat_raw.shape[1]))
                img_heatmap[0] = heat_raw
                img_tensor = transforms.ToPILImage()(torch.Tensor(img_heatmap))
                img_heatmap = transforms.Resize((512,512))(img_tensor)
                img_heatmap = np.uint8(matplotlib.cm.get_cmap('rainbow')(np.array(img_heatmap)) * 255)
                heatmap_tag = str(epoch)+'_'+str(i)+'_'+str(j)+'_heatmap_'+classes[k]
                # change channel order to Visdom style
                vis.image(np.transpose(img_heatmap,(2,0,1)), opts=dict(title=heatmap_tag))
                # write to Tensorboard
                heatmap[0]=img_heatmap  
                my_logger.image_summary(tag='/train/'+heatmap_tag, images=heatmap, step=step)
    return step

def validate(val_loader, model, criterion, epoch, my_logger, vis):
    batch_time = AverageMeter()
    losses = AverageMeter()
    avg_m1 = AverageMeter()
    avg_m2 = AverageMeter()


    # switch to evaluate mode
    model.eval()

    end = time.time()
    for i, (input, target) in enumerate(val_loader):
        step = epoch * len(val_loader) + i
        
        target = target.type(torch.FloatTensor).cuda(async=True)
        input_var = torch.autograd.Variable(input, volatile=True)
        target_var = torch.autograd.Variable(target, volatile=True)

        # TODO: Get output from model
        # TODO: Perform any necessary functions on the output
        # TODO: Compute loss using ``criterion``
        # compute output
        feature=model(input_var)
        maxpooling = nn.MaxPool2d(kernel_size=(feature.size()[2], feature.size()[3]))
        output = maxpooling(feature)
        output = output.view(output.shape[0], 20)
        imoutput = F.sigmoid(output)
        loss=criterion(imoutput,target_var)


        # measure metrics and record loss
        m1 = metric1(imoutput.data, target)
        m2 = metric2(imoutput.data, target)
        losses.update(loss.data[0], input.size(0))
        avg_m1.update(m1[0], input.size(0))
        avg_m2.update(m2[0], input.size(0))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq == 0:
            print('Test: [{0}/{1}]\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                  'Metric1 {avg_m1.val:.3f} ({avg_m1.avg:.3f})\t'
                  'Metric2 {avg_m2.val:.3f} ({avg_m2.avg:.3f})'.format(
                   i, len(val_loader), batch_time=batch_time, loss=losses,
                   avg_m1=avg_m1, avg_m2=avg_m2))

        #TODO: Visualize things as mentioned in handout
        #TODO: Visualize at appropriate intervals
#             my_logger.scalar_summary(tag='validate/metric1', value=avg_m1.avg, step=step)
#             my_logger.scalar_summary(tag='validate/metric2', value=avg_m2.avg, step=step)

        mean=[0.485, 0.456, 0.406]
        std=[0.229, 0.224, 0.225]
        classes = val_loader.dataset.classes
        if  i==0:
            batch_size=feature.shape[0]
            for j in range(20):
                k=int(np.nonzero(target[j])[0])
                img_raw=input.cpu().numpy()[j]
                f=feature.data.cpu().numpy()[j]
                #Denormalize
                for c in range(3):
                    img_raw[c] = img_raw[c]*std[c]+mean[c]

                img_raw_t = np.transpose(img_raw, (1, 2, 0))
                img = np.zeros((1, 512, 512, 3))
                img[0] = img_raw_t
                img_tag = str(epoch)+'_'+str(i)+'_'+str(j)+'_image'
                my_logger.image_summary(tag='/validate/'+img_tag, images=img, step=step)
                vis.image(img_raw, opts=dict(title=img_tag)) 

                out=imoutput.data.cpu().numpy()[j]
                out_idx=np.where(out==max(out))[0][0]

                heat_raw=f[out_idx]
                heat_raw = (heat_raw - heat_raw.min()) / (heat_raw.max() - heat_raw.min())
                heatmap=np.zeros((1, 512, 512, 4))
                img_heatmap = np.zeros((1, heat_raw.shape[0], heat_raw.shape[1]))
                img_heatmap[0] = heat_raw
                img_tensor = transforms.ToPILImage()(torch.Tensor(img_heatmap))
                img_heatmap = transforms.Resize((512,512))(img_tensor)
                img_heatmap = np.uint8(matplotlib.cm.get_cmap('rainbow')(np.array(img_heatmap)) * 255)
                heatmap_tag = str(epoch)+'_'+str(i)+'_'+str(j)+'_heatmap_'+classes[k]
                # change channel order to Visdom style
                vis.image(np.transpose(img_heatmap,(2,0,1)), opts=dict(title=heatmap_tag))
                # write to Tensorboard
                heatmap[0]=img_heatmap  
                my_logger.image_summary(tag='/validate/'+heatmap_tag, images=heatmap, step=step)


    print(' * Metric1 {avg_m1.avg:.3f} Metric2 {avg_m2.avg:.3f}'
          .format(avg_m1=avg_m1, avg_m2=avg_m2))

    return avg_m1.avg, avg_m2.avg


# TODO: You can make changes to this function if you wish (not necessary)
def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, 'model_best.pth.tar')


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def adjust_learning_rate(optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = args.lr * (0.1 ** (epoch // 30))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


def metric1(output, target):
    # TODO: Ignore for now - proceed till instructed
    # mAP
    batch_size = target.size(0)
    class_types = target.size(1)
    output = output.cpu().numpy().astype('float32')
    target = target.cpu().numpy().astype('float32')
    mAP = 0.0
    for i in range(batch_size):
        pred_cls = output[i]
        gt_cls = target[i]
        
        ap = sklearn.metrics.average_precision_score(
            gt_cls, pred_cls, average=None)
        mAP += ap
    return [mAP/batch_size]
    #return [0]


def metric2(output, target):
    # TODO: Ignore for now - proceed till instructed
    # Top 5
    batch_size = target.size(0)
    class_types = target.size(1)
    top5 = 0.0
    for i in range(batch_size):
        pred_cls = output[i]
        idx = sorted(range(class_types), key=lambda j: pred_cls[j], reverse=True)
        temp = 0.0
        for k in range(5):
            temp += output[i,idx[k]] * target[i,idx[k]]
        if temp != 0:
            top5 += 1.0
    return [top5/batch_size]
    #return [0]

if __name__ == '__main__':
    main()
