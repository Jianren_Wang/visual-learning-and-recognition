import visdom
import numpy as np
import _init_paths
import cv2

from datasets.factory import get_imdb
imdb=get_imdb('voc_2007_test')
vis=visdom.Visdom(port='8097')

path=imdb.image_path_from_index('002018')
ind=imdb._load_image_set_index().index('002018')

gt=imdb.gt_roidb()
box_rp=imdb._load_selective_search_roidb(gt)[ind]['boxes']

img1=cv2.imread(path,1)
for i in range(0,10):
    cv2.rectangle(img1,(box_rp[i][0],box_rp[i][1]),(box_rp[i][2],box_rp[i][3]),(0,255,0),1)
img1=img1[:,:,-1::-1]
img1=np.transpose(img1,(2,0,1))
vis.image(img1)

img2=cv2.imread(path,1)
box_gt=imdb._load_pascal_annotation('002018')['boxes']
for i in range(0,box_gt.shape[0]):
    cv2.rectangle(img2,(box_gt[i][0],box_gt[i][1]),(box_gt[i][2],box_gt[i][3]),(0,255,0),1)
img2=img2[:,:,-1::-1]
img2=np.transpose(img2,(2,0,1))
vis.image(img2)


